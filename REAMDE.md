# E-education

## Possible causes of Django 500 error in non-debug mode

- media folder is not writable
- imagekit: images not generated
- translation messages not compiled
- haystack: not indexed
