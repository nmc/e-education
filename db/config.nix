{ dbIP, bridgeIP, dbPass, djangoIP, qemuIP }: { pkgs, config ? <nixos-config>, ... }: {
  environment.systemPackages = with pkgs; [watch];
  networking.defaultGateway = bridgeIP;
  networking.firewall.enable = false;
  networking.interfaces.eth0.ipv4.addresses = [{
    address = dbIP;
    prefixLength = 24;
  }];
  networking.nameservers = [ "131.152.1.1" "131.152.1.5" ];
  networking.search = [ "nmc.unibas.ch" ];
  networking.useDHCP = false;
  services.mysql = {
    enable = true;
    # the default is to bind to all addresses
    # bind = "10.0.3.5";
    package = pkgs.mariadb;
    initialDatabases = [{
      name = "nmc_eEducation";
      schema = ./2021_08_30.dump;
    }];
    initialScript = pkgs.writeText "grantPermissions.sql" ''
      GRANT ALL PRIVILEGES ON nmc_eEducation.* TO 'nmc_eEducation'@'${bridgeIP}' IDENTIFIED BY '${dbPass}' WITH GRANT OPTION;
      GRANT ALL PRIVILEGES ON nmc_eEducation.* TO 'nmc_eEducation'@'${djangoIP}' IDENTIFIED BY '${dbPass}' WITH GRANT OPTION;
      GRANT ALL PRIVILEGES ON nmc_eEducation.* TO 'nmc_eEducation'@'${qemuIP}' IDENTIFIED BY '${dbPass}' WITH GRANT OPTION;
    '';
    ensureDatabases = [ "nmc_eEducation" ];
    ensureUsers = [{
      name = "nmc_eEducation";
      ensurePermissions = {
        "nmc_eEducation.*" = "ALL PRIVILEGES";
      };
    }];
    extraOptions = ''
      skip-host-cache
      skip-name-resolve
      log_error = /var/log/mysql/mysql_error.log
      general_log_file = /var/log/mysql/mysql.log
      general_log = 1
    '';
  };
  systemd.tmpfiles.rules = [
    "Z /var/log/mysql/ 0700 mysql - - -"
  ];
  security.sudo.configFile = "%wheel ALL=(ALL) ALL";
  services.openssh.enable = true;
  users = {
    mutableUsers = false;
    users.nmc-schnauz = {
      extraGroups = [ "wheel" ];
      home = "/home/nmc-schnauz";
      isNormalUser = true;
      password = "bling";
    };
  };
}
