{ "20.09" =
    let
      release = "20.09";
      revision = "89a06218ca0b";
    in builtins.fetchTarball {
      name = "nixpkgs-${release}.${revision}";
      url = "https://github.com/nixos/nixpkgs/archive/${revision}.tar.gz";
      sha256 = "0lzqs2zzsj3qfnmxz2whap9wjqgff60j30r1qwav3vh89xgl45iz";
    };
  "21.05" =
    let
      release = "21.05";
      revision = "6613a30c5e3e";
    in builtins.fetchTarball {
      name = "nixpkgs-${release}.${revision}";
      url = "https://github.com/nixos/nixpkgs/archive/${revision}.tar.gz";
      sha256 = "18v74cwjcl7qkdhgc8xic9fvp3330dsc82ah4xs3qzl5ks2h9d5h";
    };
}
