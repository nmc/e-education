let
  config = { config, lib, pkgs, ... }: with pkgs; {
    imports = [
      <nixpkgs/nixos/modules/virtualisation/qemu-vm.nix>
    ];
    fileSystems."/eedu/project" = {
        device = "project"; # this is the mount_tag name specified in qemu --virtfs parameter
        fsType = "9p";
        options = [ "trans=virtio" "version=9p2000.L" ] ++ lib.optional (cfg.msize != null) "msize=${toString cfg.msize}";
        neededForBoot = true;
    };
    environment.systemPackages = with pkgs; [
      docker-compose
      mariadb-client
      vim
      qemu
      (pkgs.writeShellScriptBin "lxcMountDisksInsideQemu"
        ''
          # mount -t 9p -o msize=8193,trans=virtio project /eedu/project -oversion=9p2000.L
          # mount -t 9p -o rw,file_mode=0777,dir_mode=0777,uid=1000,username=nmc-schnauz,msize=8193,trans=virtio media /eedu/media -oversion=9p2000.L
          mount -t 9p -o msize=8193,trans=virtio static /eedu/static -oversion=9p2000.L
        ''
      ) ];
    networking.defaultGateway = {
      # address = "10.0.2.1";
      address = "10.13.12.1";
      interface = "eth0";
    };
    networking.firewall.enable = false;
    networking.nameservers = [ "10.0.0.1" "208.67.222.222" ];
    networking.useDHCP = false;
    networking.interfaces = {
      eth0.useDHCP = false;
      eth0.ipv4.addresses = [{
        address = "10.13.12.33";
        prefixLength = 24;
      }];
    };
    users.users.nmc-schnauz = {
      isNormalUser = true;
      createHome = true;
      home = "/home/nmc-schnauz";
      password = "bling";
      uid = 1000;
      extraGroups = [ "wheel" "docker" ];
      shell = pkgs.zsh;
    };
    security.sudo = {
      enable = true;
      wheelNeedsPassword = false;
    };
    services.sshd.enable = true;
    virtualisation.msize = 8193;
    virtualisation.memorySize = 8193;
    virtualisation.diskSize = 4196;
    virtualisation.docker.enable = true;
    virtualisation.qemu = {
      # drives = [{
      #     file = "/private/nmc/e-education/media.qcow2";
      # }];
      networkingOptions = [
        "-nic tap,ifname=tapEedu,script=no,downscript=no,model=virtio-net-pci"
      ];
      options = [
        "-display none"
        # "-nographic"
        # security_model=mapped-xattr does not allow symbolic linked files
        # "-virtfs local,path=${./db},security_model=mapped-xattr,mount_tag=db"
        # "-virtfs local,path=${./.},security_model=mapped-xattr,mount_tag=project"
        # "-virtfs local,path=${./media},security_model=mapped,mount_tag=media"
        "-virtfs local,path=${./collected_static},security_model=mapped-xattr,mount_tag=static"
      ];
    };
  };
  evalNixos = configuration: import <nixpkgs/nixos> {
    inherit configuration;
  };
in (evalNixos config).config.system.build.vm
