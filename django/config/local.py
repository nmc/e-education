import os
from .common import *
# DEBUG = False
INSTALLED_APPS += ( 'debug_toolbar', )
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
TEMPLATES[0]['OPTIONS']['context_processors'] += ('django.template.context_processors.debug',)
# MIDDLEWARE += ( 'debug_toolbar.middleware.DebugToolbarMiddleware', )
DATABASES = {
    'default': {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "nmc_eEducation",
        "USER": "nmc_eEducation",
        "PASSWORD": os.environ['DB_PASS'],
        "HOST": os.environ['DB_HOST_LOCALLY'],
        "PORT": "3306",
        "ATOMIC_REQUESTS": True
    }
}

# needed for debug tools
INTERNAL_IPS = ['127.0.0.1',]

MEDIA_ROOT = os.path.join(BASE_DIR, "../media")

OIDC_OP_AUTHORIZATION_ENDPOINT = 'https://login.test.eduid.ch/idp/profile/oidc/authorize'
OIDC_OP_JWKS_ENDPOINT = 'https://login.test.eduid.ch/idp/profile/oidc/keyset'
OIDC_OP_TOKEN_ENDPOINT = 'https://login.test.eduid.ch/idp/profile/oidc/token'
OIDC_OP_USER_ENDPOINT = 'https://login.test.eduid.ch/idp/profile/oidc/userinfo'
OIDC_RP_SCOPES = "openid email swissEduIDExtended"
OIDC_RP_CLIENT_SECRET = os.environ['OIDC_CLIENT_SECRET']

# when serving static files, django searches this folder:
# ADDITIONALLY to STATIC_ROOT, so not needed in our case
# STATICFILES_DIRS = [os.path.join(BASE_DIR, "../static")]
# django collectstatic will put the files into this folder:
STATIC_ROOT = os.path.join(BASE_DIR, "../collected_static")
