from .common import *
DATABASES = {
  'default': {
    "ENGINE": "django.db.backends.mysql",
    "NAME": "nmc_eEducation",
    "USER": "nmc_eEducation",
    "PASSWORD": os.environ['DB_PASS'],
    "HOST": os.environ['DB_HOST_DOCKER_COMPOSE'],
    "PORT": "3306",
    "ATOMIC_REQUESTS": True
  }
}

# even though media is served by nginx, django needs direct access for template rendering
MEDIA_ROOT = '/opt/media'

OIDC_OP_AUTHORIZATION_ENDPOINT = 'https://login.eduid.ch/idp/profile/oidc/authorize'
OIDC_OP_JWKS_ENDPOINT = 'https://login.eduid.ch/idp/profile/oidc/keyset'
OIDC_OP_TOKEN_ENDPOINT = 'https://login.eduid.ch/idp/profile/oidc/token'
OIDC_OP_USER_ENDPOINT = 'https://login.eduid.ch/idp/profile/oidc/userinfo'
OIDC_RP_SCOPES = "openid email"
OIDC_RP_CLIENT_SECRET = os.environ['OIDC_CLIENT_SECRET']
