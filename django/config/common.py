import os
from pathlib import Path
from django.utils.translation import ugettext_lazy as _

ALLOWED_HOSTS = [ '*' ]

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
]

AUTHENTICATION_BACKENDS = [
    'mozilla_django_oidc.auth.OIDCAuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend'
]

BASE_DIR = Path(__file__).resolve().parent.parent

CRISPY_TEMPLATE_PACK = 'bootstrap3'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

EMAIL_HOST = "smtp.unibas.ch"
EMAIL_PORT = 25

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'xapian_backend.XapianEngine',
        'PATH': os.path.join(BASE_DIR, 'xapian_index'),
        'INCLUDE_SPELLING': True,
    },
}
HAYSTACK_CUSTOM_HIGHLIGHTER = \
    'educationalresources.utils.highlighting.EEducationHighlighter'
HAYSTACK_SEARCH_RESULTS_PER_PAGE = 0

HAYSTACK_SIGNAL_PROCESSOR = 'server.haystackSignal.LRSignalProcessor'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mozilla_django_oidc',
    'captcha',
    'crispy_forms',
    'embed_video',
    'floppyforms',
    'haystack',
    'imagekit',
    'linkcheck',
    'simple_history',
    'server.apps.ServerConfig'
]

LANGUAGE_CODE = 'en'
ugettext = lambda s: s
LANGUAGES = (
    ('de', _('German')),
    ('en', _('English')),
)
LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'server/locale'),
]

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            # 'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
    }
}

LOGIN_REDIRECT_URL = '/admin/auth/user'
MEDIA_URL = '/media/'

OIDC_RP_CLIENT_ID = 'unibas-eedu-registration'
OIDC_RP_SIGN_ALGO = 'RS256'
OIDC_TOKEN_USE_BASIC_AUTH = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',
]

RECAPTCHA_PUBLIC_KEY = '6LddZSgTAAAAAGOnZIf1cWJudLnxnzatCuOKHAPE'
RECAPTCHA_PRIVATE_KEY = '6LddZSgTAAAAAPnHMG-OD35YuymGsUHc3k0lIy_z'
RECAPTCHA_USE_SSL = True
NOCAPTCHA = True

ROOT_URLCONF = 'server.urls'

SITE_DOMAIN = "unibas.ch"
STATIC_URL = '/static/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
TIME_ZONE = 'UTC'

USE_I18N = True
USE_L10N = True
USE_TZ = True

SECRET_KEY = os.environ['DJANGO_SECRET']
