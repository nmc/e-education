from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.template.defaultfilters import slugify
from django.utils.html import escape
from django.utils.html import mark_safe

from simple_history.models import HistoricalRecords

from embed_video.fields import EmbedVideoField

import itertools

from .imagegenerators import Thumbnail
from . import validators
from . import fields


class Language(models.Model):
    title = models.CharField(max_length=100)
    short_code = models.CharField(max_length=4)

    history = HistoricalRecords()

    """
    return an unicode representation of the resource
    """
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Language')
        verbose_name_plural = _('Languages')


class Rights(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=3000)
    rights_link = models.URLField(max_length=201, blank=True)

    history = HistoricalRecords()

    RIGHTS_REUSE = 1
    RIGHTS_REUSE_NO_DERIVATES = 2
    RIGHTS_REUSE_NONCOMMERCIAL = 3
    RIGHTS_REUSE_NO_DERIVATES_NONCOMMERCIAL = 4
    RIGHTS_NOT_CLEAR = 5

    RIGHTS_CHOICES = (
            (RIGHTS_REUSE, _('''Reuse with modification allowed''')),
            (RIGHTS_REUSE_NO_DERIVATES, _('''Reuse allowed''')),
            (RIGHTS_REUSE_NONCOMMERCIAL, _(
                '''Noncommercial reuse with modification allowed'''
            )),
            (RIGHTS_REUSE_NO_DERIVATES_NONCOMMERCIAL, _(
                '''Noncommercial reuse allowed'''
            )),
            (RIGHTS_NOT_CLEAR, _('''Rights unknown''')),
    )
    rights_type = models.IntegerField(
        choices=RIGHTS_CHOICES, default=RIGHTS_NOT_CLEAR
    )

    @staticmethod
    def get_default_rights():
        default_rights, created = Rights.objects.get_or_create(

            title=_("Rights unknown"),

            rights_type=Rights.RIGHTS_NOT_CLEAR
        )
        return default_rights

    """
    return an unicode representation of the resource
    """
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Rights')
        verbose_name_plural = _('Rights')


class Type(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=3000)

    history = HistoricalRecords()

    """
    return an unicode representation of the type
    """
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Type')
        verbose_name_plural = _('Types')


class Subject(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=3000)

    history = HistoricalRecords()

    """
    return an unicode representation of the type
    """
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')


class LearningResource(models.Model):
    """
    This is the most important class of the eEducation project. It aims to map
    """
    # identifier: Eindeutige Identifizierung des Dokuments nach einem passenden
    # Katalog, beispielsweise ISBN/ISSN, URL/PURL, URN oder DOI, sowie ggf. den
    # Eintrag (bibliographicCitation), wie das Dokument zu zitieren ist.
    id = models.AutoField(primary_key=True)
    # irrelevant: identifier TODO: remove field identifier
    identifier = models.CharField(max_length=200, help_text=_(
        '''An unambiguous reference to the resource within a given context.
        Recommended best practice is to identify the resource by means of a
        string or number conforming to a formal identification system.'''
    ))

    # Description of content
    # title: Titel des Dokuments, unter dem es „formell bekannt“ gemacht wird.
    # Das Feld wird von anzeigenden Programmen gerne in den Titelbalken
    # übernommen. Abkürzungen oder auch Übersetzungen des Titels werden
    # alternativ zum formellen Titel (alternative) angegeben.
    title = models.CharField(
        max_length=200,
        verbose_name=_("Title"),
        help_text=_(
            '''The name given to the resource. Typically, a Title will be a name by
        which the resource is formally known.''')
    )
    # subtitle: subtitle
    subtitle = models.CharField(max_length=200, blank=True, null=True,
                                verbose_name=_("Subtitle"))

    # subject: Thema des Inhalts in suchtauglichen Schlagwörtern (keyword), die
    # bestenfalls einem formalen Klassifikationsschema folgen.
    subjects = models.ManyToManyField(
        Subject,
        related_name=_('learningResources'),
        verbose_name=_("Subject"),
        help_text=_(
            '''The topic of the content of the resource. Typically, a Subject will
        be expressed as keywords or key phrases or classification codes that
        describe the topic of the resource. Recommended best practice is to
        select a value from a controlled vocabulary or formal classification
        scheme.''')
        )

    # coverage: Eingrenzung des vom Dokument inhaltlich abgedeckten Bereichs
    # allgemein, räumlich/örtlich beispielsweise durch Namen nach TGN, auch
    # durch Angabe von Koordinaten (spatial) oder zeitlich (temporal) in Zahlen
    # unter Benennung einer Ära oder Periode.
    coverage = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_("Coverage"),
        help_text=_(
            '''The extent or scope of the content of the resource. Coverage will
        typically include spatial location (a place name or geographic
        co-ordinates), temporal period (a period label, date, or date range)
        or jurisdiction (such as a named administrative entity). Recommended
        best practice is to select a value from a controlled vocabulary''')
    )

    # description: Kurzzusammenfassung des Inhalts des Dokuments in Freitext
    # (abstract), Kopie des Inhaltsverzeichnisses bzw. Liste der Bestandteile
    # (tableOfContents) oder als Verweis auf eine beschreibende Quelle. Das
    # Feld wird in Dialogen von bearbeitenden Programmen gerne als „Kommentar“
    # gelistet.
    description = models.TextField(
        max_length=3000,
        verbose_name=_("Description"),
        help_text=_(
            '''An account of the content of the resource. Description may include
        but is not limited to: an abstract, table of contents, reference to a
        graphical representation of content or a free-text account of the
        content.''')
    )

    # Description of technical data
    # type: Art bzw. Gattung des Dokuments, bezeichnet am besten mithilfe eines
    # Terminus:
    # Interaktives Dokument („Interactive Resource“), das Benutzereingaben
    # erfordert, z. B. ein Formular.
    # Programm („Software“) als Quelltext oder in ausführbarer Form, soweit es
    # nicht ein interaktives Dokument ist, sondern für die dauerhafte
    # Installation.
    # Datensatz („Dataset“) in spezieller, definierter Kodierung, die für die
    # maschinelle Weiterverarbeitung gedacht ist.
    # Dienste oder Dienstleistungen („Service“), wie ein Copy-Shop oder auch
    # ein Web-Server.
    types = models.ManyToManyField(
        Type,
        related_name=_('learningResources'),
        verbose_name=_("Types"),
        help_text=_(
            '''The nature or genre of the content of the resource. Type includes
            terms describing general categories, functions, genres, or
            aggregation levels for content. Recommended best practice is to
            select a value from a controlled vocabulary (for example, the
            DCMIType vocabulary ).  To describe the physical or digital
            manifestation of the resource, use the FORMAT element.'''
        )
    )

    # format: Die Formatangabe soll eine Hilfe geben, womit das Dokument
    # dargestellt bzw. weiterverarbeitet werden kann; Größe oder Laufzeit als
    # (extent); Medientyp (medium) als Angabe des physikalischen Datenträgers
    # oder Materials, auf digitale Inhalte bezogen sinnvollerweise als MIME-Typ
    # anzugeben.
    # irrelevant: type TODO: remove field type
    format = models.CharField(
        max_length=200, null=True, blank=True, help_text=_(
            '''The physical or digital manifestation of the resource. Typically,
            Format may include the media-type or dimensions of the resource.
            Examples of dimensions include size and duration. Format may be
            used to determine the software, hardware or other equipment needed
            to display or operate the resource.'''
        )
    )

    # language: Sprache des Dokumenteninhalts. Empfohlen wird ein Sprachkürzel
    # nach ISO 639 (en,de,fr,...)
    languages = models.ManyToManyField(
        Language,
        related_name=_('learningResources'),
        verbose_name=_("Languages"),
        help_text=_(
            '''A language of the intellectual content of the resource.
            Recommended best practice for the values of the Language element is
            defined by ISO 639 (en, de, fr, ...)'''
        )
    )

    # People and rights
    # creator: Nach DCMI der Name derjenigen Person oder Organisation, die
    # vorrangig für die Anfertigung des Dokuments verantwortlich ist, also der
    # verantwortliche Verfasser oder Urheber (vgl. source, contributor sowie
    # publisher) des Dokuments in seiner aktuellen Fassung. Das sollte, muss
    # aber nicht, eine natürliche Person sein.
    creator = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Creator"),
        help_text=_(
            '''An entity primarily responsible for making the content of the
            resource. Examples of a Creator include a person, an organization,
            or a service. Typically the name of the Creator should be used to
            indicate the entity.'''
        )
    )

    # publisher: Name der veröffentlichenden Instanz, typischerweise der
    # Verleger oder Herausgeber. Auch hier muss nicht, aber sollte die
    # verantwortliche natürliche Person benannt werden.

    # irrelevant: publisher TODO: remove field publisher
    publisher = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_("Publisher"),
        help_text=_(
            '''The entity responsible for making the resource available.
            Examples of a Publisher include a person, an organization, or a
            service. Typically, the name of a Publisher should be used to
            indicate the entity.'''
        )
    )

    # contributor: Name je einer weiteren Person, die zur Erstellung des
    # Dokuments einen Beitrag geleistet bzw. verantwortet hat.
    contributor = models.CharField(
        max_length=2000,
        blank=True,
        verbose_name=_("Contributor"),
        help_text=_(
            '''An entity responsible for making contributions to the content of
            the resource. Examples of a Contributor include a person, an
            organization or a service. Typically, the name of a Contributor
            should be used to indicate the entity.'''
        )
    )

    # rights: Information zur Klarstellung der Rechte, die an dem Dokument
    # gehalten werden oder dies betreffend zu beachten sind, als Direktangabe,
    # in Form eines Verweises (URI) auf Lizenzbedingungen (license) oder
    # Rechteverwerter. Unter (accessRights) wird der Sicherheitsstatus des
    # Dokuments spezifiziert und wer Zugriff darauf hat bzw. haben darf.
    rights = models.ForeignKey(
        Rights,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("Rights"),
        help_text=_(
            '''Information about rights held in and over the resource. Typically
            a Rights element will contain a rights management statement for the
            resource, or reference a service providing such information. Rights
            information often encompasses Intellectual Property Rights (IPR),
            Copyright, and various Property Rights. If the rights element is
            absent, no assumptions can be made about the status of these and
            other rights with respect to the resource.'''
        )
    )

    # rightsHolder: Name der Person oder Organisation, die Eigner oder
    # Verwerter der Rechte an diesem Dokument ist.
    # irrelevant: rightsHolder TODO: remove field rightsHolder
    rightsHolder = models.CharField(
        max_length=200, blank=True, help_text=_(
            '''A person or organization owning or managing rights over the
            resource. Recommended best practice is to use the URI or name of
            the Rights Holder to indicate the entity.'''
        )
    )

    # Relation
    # source: Verweist (vgl. o. Identifier) auf ein Dokument, von dem das
    # aktuell beschriebene Dokument ganz oder in Teilen abgeleitet wurde.
    source = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_("Source"),
        help_text=_(
            '''A Reference to a resource from which the present resource is
            derived. The present resource may be derived from the Source
            resource in whole or part. Recommended best practice is to
            reference the resource by means of a string or number conforming to
            a formal identification system.'''
        )
    )

    # relation: Verweist (vgl. o. Identifier) auf ein Dokument, mit dem das
    # beschriebene Dokument in Beziehung steht.
    relation = models.CharField(
        max_length=200,
        blank=True,
        default="",
        verbose_name=_("Relation"),
        help_text=_(
            '''A reference to a related resource. Recommended best practice is
            to reference the resource by means of a string or number conforming
            to a formal identification system.'''
        )
    )

    # audience: Klassifizierung der Zielgruppe des Dokuments, nach Vorgabe des
    # creator, des publisher oder von dritter Seite. Auf Seite des
    # Endrezipienten (z. B. Schüler) beispielsweise nach Fortschritt innerhalb
    # eines Ausbildungsprogramms (educationLevel) oder auf der Gegenseite eine
    # Klassifizierung der Instanz (mediator), die den Zugriff auf das Dokument
    # vermittelt (z. B. Lehrer).
    audience = models.CharField(
        max_length=200,
        blank=True,
        default="",
        verbose_name=_("Audience"),
        help_text=_(
            '''A class of entity for whom the resource is intended or useful. A
            class of entity may be determined by the creator or the publisher
            or by a third party.'''
        )
    )

    # Lifecycle
    # date: Ein charakteristisches Datum oder auch eine Zeitspanne im
    # Lebenszyklus des Dokuments, sinnvollerweise in Notation nach DIN ISO 8601
    # als JJJJ-MM-TT, auch im Sinne von Bearbeitungsvermerken und durchaus auch
    # in die Zukunft gerichtet. Im Zweifelsfall das Datum der letzten
    # Dokumentänderung.
    date = models.DateTimeField(
        verbose_name=_("Date"),
        help_text=_(
            '''A date associated with an event in the life cycle of the
            resource. Typically, Date will be associated with the creation or
            availability of the resource.'''
        )
    )

    # Additional fields
    supporters = models.TextField(
        verbose_name=_("Supporters"),
        max_length=3000,
        blank=True,
        null=True,
        help_text=_(
            '''A comma separated list of persons or organizations which
            supported the creation or maintenance this resource in any way.''')
    )

    link = models.URLField(max_length=400, help_text=_(
        '''Is there a link to the resource on the web?''')
    )

    title_image = models.ImageField(
        upload_to='title_images', blank=True,
        verbose_name=_("Title Image"),
        validators=[validators.validate_image],
    )

    frontpage = models.BooleanField(
        default=False,
        help_text=_(
            '''Display this resource on the frontpage'''
        ),
        verbose_name=_("Display on frontpage")
    )

    trailer = EmbedVideoField(max_length=400, blank=True, help_text=_(
        '''Paste in you Youtube, Vimeo, or nmc video url''')
    )

    has_technical_problems = models.BooleanField(
        default=False,
        verbose_name=_("Has Technical Problems"),
        help_text=_(
            '''This resource has technical problems. (It uses outdated technology,
        old browser plugins, etc.)''')
    )

    # slug: the human readable url under which the resource is accessible
    slug = models.SlugField(unique=True)

    # oai_id: The id of the resource through which it can be syncronised with
    # aleph
    oai_id = fields.CharNullField(
        max_length=100, blank=True, null=True, unique=True, help_text=_(
            '''the id of the resource through which it can be syncronised with
            aleph'''
        )
    )

    # sru_id: The id of the resource through which it can be syncronised with
    # aleph
    sru_id = fields.CharNullField(
        max_length=100, blank=True, null=True, unique=True, help_text=_(
            '''the id of the resource through which it can be syncronised with
            swissbib'''
        )
    )

    # an indicator of the popularity of the resource (currently implemented as
    # an accesscounter)
    popularity = models.PositiveIntegerField(default=0,
                                             blank=False,
                                             null=False)

    # access_type: are there any access restrictions to the resource?
    LOGIN_NO_LOGIN_REQUIRED = 1
    LOGIN_LOGIN_REQUIRED = 2
    LOGIN_CHOICES = (
        (LOGIN_NO_LOGIN_REQUIRED, _('''No login required''')),
        (LOGIN_LOGIN_REQUIRED, _('''Login required''')),
    )
    access_type = models.IntegerField(
        choices=LOGIN_CHOICES,
        default=LOGIN_NO_LOGIN_REQUIRED,
        verbose_name=_("Access type"),
    )

    published = models.BooleanField(
        default=False,
        verbose_name=_("Published"),
    )

    production = models.TextField(
        verbose_name=_("Production"),
        max_length=3000,
        blank=True,
        null=True,
        help_text=_(
            '''A field that contains additional information about the producer
            of a given ressource.''')
    )

    history = HistoricalRecords()

    def save(self, *args, **kwargs):
        """
        create slug when saving
        """
        max_length = LearningResource._meta.get_field('slug').max_length

        def create_slug():
            slug = orig = slugify(self.title)[:max_length]
            for x in itertools.count(1):
                if not LearningResource.objects.exclude(
                    id__exact=self.id
                ).filter(slug__exact=self.slug).exists():
                    break
                # Truncate the original slug dynamically. Minus 1 for the
                # hyphen.
                slug = "%s-%d" % (orig[:max_length - len(str(x)) - 1], x)
            return slug

        # Don't recreate slug everytime an object is saved
        if self.pk is not None:
            orig = LearningResource.objects.get(pk=self.pk)
            if orig.title != self.title:
                # Title has changed, recreate slug
                self.slug = create_slug()
        else:
            # Object is being created -> create slug
            self.slug = create_slug()

        # add default value to rights field
        if self.rights is None:
            self.rights = Rights.get_default_rights()

        super(LearningResource, self).save(*args, **kwargs)

    """
    create image tag
    """
    def image_tag(self):
        # TODO: Ugly hack, cleanup!
        image_generator = Thumbnail(self.title_image)
        thumbnail_image = image_generator.generate()
        destination = open(
            self.title_image.path + 'thumb', 'wb'
        )
        destination.write(thumbnail_image.read())
        destination.close()
        return mark_safe('<img src="{0}" />'.format(escape(self.image.url + 'thumb')))
    image_tag.short_description = _('Image')
    image_tag.allow_tags = True

    """
    return the absolute URL of the resource
    """
    def get_absolute_url(self):
        return reverse('resource', args=[str(self.slug)])

    """
    return an unicode representation of the resource
            """
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Learning resource')
        verbose_name_plural = _('Learning resources')


class ResourceImage(models.Model):
    """
    This class provides images to the base class
    """
    resource = models.ForeignKey(
        LearningResource,
        on_delete=models.CASCADE
    )
    image = models.ImageField(upload_to='resource_images')

    history = HistoricalRecords()

    def image_tag(self):
        # TODO: Ugly hack, cleanup!
        image_generator = Thumbnail(self.image)
        thumbnail_image = image_generator.generate()
        destination = open(self.image.path + 'thumb', 'wb')
        destination.write(thumbnail_image.read())
        destination.close()
        return mark_safe('<img src="{0}" />'.format(escape(self.image.url + 'thumb')))

    image_tag.short_description = _('Image')
    image_tag.allow_tags = True

    """
    return an unicode representation of the resource
    """
    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')
