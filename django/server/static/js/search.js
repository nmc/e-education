/* Remove URL parameter utility function */
function removeURLParameter(url, parameter) {
  //prefer to use l.search if you have a location/link object
  var urlparts= url.split('?');
  if (urlparts.length>=2) {

    var prefix= encodeURIComponent(parameter)+'=';
    var pars= urlparts[1].split(/[&;]/g);

    //reverse iteration as may be destructive
    for (var i= pars.length; i-- > 0;) {
      //idiom for string.startsWith
      if (pars[i].lastIndexOf(prefix, 0) !== -1) {
        pars.splice(i, 1);
      }
    }

    url= urlparts[0]+'?'+pars.join('&');
    return url;
  } else {
    return url;
  }
}

/* Clear all facets */
function clearFacets(){
  // Remove facet url parameter
  var urlString = removeURLParameter(window.location.href, "selected_facets");
  // Redirect to new url
  window.location.href = urlString
}

/* Remove a facet by its value */
function clearFacetByValue(value){
  var urlString = window.location.href;
  var encodedValue = encodeURIComponent(value);
  var valuePosition = urlString.indexOf(encodedValue);
  var ampersandPosition = urlString.lastIndexOf('&',valuePosition);
  var cleanUrl = urlString.slice(0, ampersandPosition) + urlString.slice(valuePosition + encodedValue.length, urlString.length);

  window.location.href = cleanUrl;
}

$(document).ready(function() {
  /* Facet evaluation and redirection */
  $(function(){
    // bind click event on facetting buttons
    $('button[name=facets]').on('click', function (event) {

      if(this.dataset){
        //Toggle active facets
        $('button[data-facet=\'' + this.dataset.facet + '\']').toggleClass("active");
      } else {
        //Toggle active facets
        $('button[data-facet=\'' + this.getAttribute("data-facet") + '\']').toggleClass("active");
      }

      //Get values of acive facets
      var checkValues = $('button[name=facets].active').map(function() {
        return $(this).val();
      }).get();

      //remove duplicates from checkValues
      var uniqueValues = [];
      $.each(checkValues, function(i, el){
        if($.inArray(el, uniqueValues) === -1) uniqueValues.push(el);
      });

      // if we find 'search' in the pathname, we are on the search page already.
      // If not we are on the welcome page and have to change to the search
      // page.
      var origin = window.location.origin // url
      var pathname = window.location.pathname // eg /en/search or /en/ on the welcome page
      var search = window.location.search // searchString, rest of the url
      var pathSplits = pathname.split('/')
      if (pathSplits[2] != 'search') { // we are on the welcome page, redirect to search page
        pathname = `/${pathSplits[1]}/search/`
      }
      var windowLoc = `${origin}${pathname}${search}`

      // remove facet url parameter
      var urlString = removeURLParameter(windowLoc, "selected_facets");
      if(urlString.indexOf('?')==-1) urlString += '?';

      // Create new url
      if (uniqueValues.length>0){
        for (var i=0; i<uniqueValues.length; i++){
          urlString += "&selected_facets=" + uniqueValues[i];
        }
      }

      // Redirect to new url
      window.location.href = urlString
    });
  });
});
