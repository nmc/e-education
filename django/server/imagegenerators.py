from imagekit import ImageSpec, register
from imagekit.processors import ResizeToFill
from imagekit.processors import SmartResize
from imagekit.processors import Transpose


class Thumbnail(ImageSpec):
    processors = [Transpose(), ResizeToFill(250, 250)]
    format = 'JPEG'
    options = {'quality': 80}


class FrontpageTN(ImageSpec):
    processors = [Transpose(), SmartResize(400, 260)]
    format = 'JPEG'
    options = {'quality': 80}


class FrontpageTNBig(ImageSpec):
    processors = [Transpose(), SmartResize(767, 500)]
    format = 'JPEG'
    options = {'quality': 80}

register.generator('eeducation:thumbnail', Thumbnail)
register.generator('eeducation:frontpagetn', FrontpageTN)
register.generator('eeducation:frontpagetnbig', FrontpageTNBig)
