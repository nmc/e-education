import os
import sys
import time
import traceback
import signal
sys.path.append('/srv/www/vhosts/e-education.unibas.ch/files/venv/lib/python3.8/site-packages')
sys.path.append('/srv/www/vhosts/e-education.unibas.ch/files/django')
sys.path.append('/srv/www/vhosts/e-education.unibas.ch/files/secrets')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.production')
from django.core.wsgi import get_wsgi_application


# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
try:
    application = get_wsgi_application()
except RuntimeError as re:
    print(re)
    print('handling WSGI exception')
    # Error loading applications
    if 'mod_wsgi' in sys.modules:
        traceback.print_exc()
        os.kill(os.getpid(), signal.SIGINT)
        time.sleep(2.5)
