from django.conf.urls import url
from django.contrib import admin
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template.defaultfilters import truncatewords
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from simple_history.admin import SimpleHistoryAdmin

import xmltodict

from .models import (LearningResource, Language,
                     Rights, Type, Subject, ResourceImage)


from .forms import SwisscoveryForm
from .views import SwisscoveryFormView

from .swisscoveryImport import SruDataImporter


@admin.register(Language)
@admin.register(Rights)
@admin.register(Subject)
@admin.register(Type)
@admin.register(ResourceImage)

class ResourceImageAdmin(admin.ModelAdmin):
    pass


class ResourceImageInline(admin.TabularInline):
    model = ResourceImage
    fields = ['image_tag', 'image', ]
    readonly_fields = ['image_tag', ]


# LearningResourceAdmin

def import_form_valid(form):
    """
    If the form is valid, redirect to the supplied URL.
    """
    xmlFile = form.files["file"]
    xmlData = xmlFile.read()

    dataDict = xmltodict.parse(xmlData)
    importer = SruDataImporter(dataDict['collection']['record'])
    importer.importArrayOfObjects()

    return HttpResponseRedirect('../')

@staff_member_required
def action_fileimport(request):
    return SwisscoveryFormView.as_view(
             template_name='pages/swisscovery.html',
             form_class=SwisscoveryForm,
             form_valid=import_form_valid
         )(request)
action_fileimport.short_description = _('Import resources from Swisscovery xml file')

@admin.register(LearningResource)
class LearningResourceAdmin(SimpleHistoryAdmin):
    class Media:
        css = {
            "all": ("css/admin.css",)
        }

    def truncated_description(self, obj):
        return truncatewords(obj.description, 20)
    truncated_description.short_description = _("Description")

    list_display = ('id', 'title', 'truncated_description', 'published')
    list_filter = ['published', 'frontpage', 'has_technical_problems']

    search_fields = ['title']
    readonly_fields = ['show_url', 'image_tag']
    inlines = [ResourceImageInline, ]

    actions = [action_fileimport]

    fieldsets = (
        (_('Description of the content'), {
            'fields': (
                'title', 'subtitle', 'subjects', 'description', 'sru_id'
            )
        }),
        (_('Description of technical data'), {
             'fields': ('languages', 'types')
        }),
        (_('People and rights'), {
            'fields': ('creator', 'contributor', 'supporters',
                       'production', 'rights')
        }),
        (_('Lifecycle'), {
            'fields': ('date',)
        }),
        (_('Additional fields'), {
            'fields': ('link', 'show_url', 'title_image',
                       'image_tag', 'trailer', 'frontpage', 'published',
                       'has_technical_problems', 'access_type')
        }),
    )

    def show_url(self, instance):
        try:
            url = reverse("resource",
                          kwargs={"slug": instance.slug})
            response = format_html('''<a href="{0}">{1}</a>''', url, url)
        except:
            response = format_html(_('''No URL yet.'''))

        return response

    show_url.short_description = "URL"
    show_url.allow_tags = True

    def get_urls(self):
        urls = super(LearningResourceAdmin, self).get_urls()
        my_urls = [url(r"^fileimport/$", action_fileimport)]
        return my_urls + urls
