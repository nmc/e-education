from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from PIL import Image

"""
custom validators
"""


def validate_image(fieldfile_obj):
    # Minimal image width
    min_width = 1000

    # Create image of uploaded file
    image = Image.open(fieldfile_obj.file)
    file_width = image.size[0]

    # vaildate image width
    if file_width < min_width:
        raise ValidationError("Minimal image width is %spx" % str(min_width))
