��    �      t  �   �
      `    a  �  x  �     �   �  �   h  �  ^  �   �  �   �  �   �  �   ?    (  \   .  �   �  �   >     �  H  �  �   .  �   �  �   c  a      �   v   �   /!  �   �!     �"     �"  �   �"  �   �#    �$  �   �%     �&     �&  	   �&     �&     �&     �&     �&     �&     �&     �&     �&     '     '     -'     H'  &   ]'  	   �'     �'     �'     �'     �'     �'  (   �'     �'     (     !(     &(     ,(  	   3(     =(     E(     F*  +   \*     �*     �*  	   �*     �*     �*     �*     �*     �*  	   +     +     $+     *+      9+     Z+     g+     �+     �+     �+     �+     �+     �+  -   �+  9   ,     O,     \,     j,  -   y,     �,     �,  
   �,  
   �,  	   �,  	   �,     �,     �,     -     #-     *-     9-  ;   @-     |-     �-     �-     �-     �-  
   �-     �-  H   �-  �   	.  j  �.  r   e0  �  �0  Y  �2  9  �3     5     *5  f   B5  �   �5     26     86     D6     V6     [6     a6     u6     }6  k  �6     �7  	   �7     8     8     8      8     '8     /8     A8     F8  
   R8     ]8     o8  	   u8     8     �8     �8     �8     �8     �8     �8     �8     �8     �8  	    9     
9     9     "9     49     H9  
   ]9     h9  
   o9     z9  Q   �9  T   �9     ):     =:     E:     N:     T:  B  Z:  i  �;  K  =  [   S>  �   �>  �   ??  �  �?  �   �A  1  OB  �   �C  �   mD    E  H   #F  �   lF  �   ,G  (   �G     "H  r  �H  �   J     �J  7   �K  [   L  _   iL  e   �L     /M     IM    RM  =  qN  n   �O  �   P     �P     �P     Q     Q     Q     *Q  	   /Q  	   9Q     CQ     YQ     eQ     kQ     xQ     �Q  ,   �Q  ,   �Q  
   
R  
   R      R     /R     8R     @R  M   \R     �R     �R     �R     �R     �R  	   �R  	   �R  P   S     QT  *   gT     �T     �T     �T     �T     �T  	   �T  
   �T     �T     �T     U     U     U     1U     IU     \U     |U     �U     �U     �U     �U  *   �U  ;   V  3   KV     V     �V     �V  ,   �V     �V     �V  
   W     W     W     ,W  	   8W     BW  )   [W     �W     �W     �W  6   �W     �W     �W     �W     �W  
   X  	   X     X  N   4X  �   �X  �   0Y    (Z  "  -[  "  P]  z   s^     �^     _  v    _  u   �_     `  	   `     `     3`     :`     C`     V`     o`  z  {`     �a  	   �a     b     b     b     &b  	   +b     5b     Kb     Qb     _b     mb     b  
   �b  
   �b     �b  
   �b     �b     �b     �b     �b     �b     �b     �b     c     c     c  
   /c     :c     Lc     \c     hc  
   wc     �c  (   �c  +   �c     �c     �c     �c     d     	d             A   <   �   �   n   G   1   D      �   =   d           r   �   %      ^          �   w   #                    B   �   x      !           �   /      N       �       (   3           |   U   �       E   �   k   '   P      )   u       W   �       �           �   ?   b                   2              \   �           l   �       q       �       �   �   C   �       �   v       5   h   9       8   	   �   m   7   �          j       >       �   Q         �   �              t   S   &              `   K       _       c   �   R   $   �   y   g                 �         �   X   {   �   �   .   �   L   p      �   Y   �   �              I   "      6      H           �   4       s               �       �      
   i   �       M       V   �   ,   �       z      F   �       �      f   a      :   ~   T   ]   Z   ;          +       *   e   O          J   0   o   @   -   [   �   }    
          <b>We appreciate feedback!</b> Send us your comments and suggestions
          on the portal eEducation. Please also write us if you have any
          questions about eEducation or if you want your resource to be found
          via the eEducation portal.
           
          Copyright and any other rights relating to texts, illustrations,
          photos or any other data available on the NMC website are the
          exclusive property of the NMC or of any other expressly mentioned
          owners.

          <!-- Downloading or copying of texts, illustrations, photos or any other data does not entail any transfer of rights of the content. Copyright and any other rights relating to texts, illustrations, photos or any other data available on the NMC website are the exclusive property of the NMC or of any other expressly mentioned owners. Any reproduction requires the prior written consent of the copyright holder. -->
           
          Please note that downloading or copying texts or other documents does
          not per se entail any transfer of rights to the content.
           
          When planning to use any of the resources found through this portal,
          make sure to consider the license under which it has been published.
           
        Although every care has been taken by Educational Technologies to ensure the accuracy of the information published, no warranty can be given in respect of the accuracy, reliability, currency or completeness of this information.
         
        Based on Article 13 of the Swiss Federal Constitution and the provisions of federal data protection legislation, everyone has the right to the protection of their privacy, as well as to protection from misuse of their personal details. The Educational Technologies complies with these provisions. Personal data is treated as strictly confidential and is neither passed on nor sold to third parties.
         
        In close cooperation with our hosting provider, we make every effort to protect the databases as well as possible from outside access, data loss, misuse or falsification.
         
        In no event will the Educational Technologies be liable for any loss or damage of a material or immaterial nature arising from access to, use or non-use of published information, or from misuse of the connection or technical faults.
         
        The Educational Technologies reserves the right to alter or remove content, in full or in part, without prior notice.
         
        University of Basel<br>
        Educational Technologies<br>
        Petersgraben 35<br>
        4001 Basel<br>
        Switzerland<br>
        <br>
        Tel.: +41 61 207 12 12<br>
        E-Mail: bbit@unibas.ch
         
        Use of links on our website to third party websites lies outside the scope of responsibility of the Educational Technologies. Access and use of such websites occurs entirely at the user’s own risk. The Educational Technologies expressly states that it has no influence whatsoever over the content of any linked site. Responsibility for information and services of linked sites rests entirely with the relevant third parties. No liability can be accepted in respect of the content of such websites.
         
      You can either browse through the resources or search for them using keywords.
       
    Please leave us some <a href="%(the_url)s">feedback</a>. And do not hesitate to <a href="%(the_url)s">contact us</a> if you want get your own resources onto the Portal.
     
    The portal allows access to the educational resources from different disciplines for those interested in learning and teaching.
     <h2> Send us a message </h2> A Reference to a resource from which the present resource is
            derived. The present resource may be derived from the Source
            resource in whole or part. Recommended best practice is to
            reference the resource by means of a string or number conforming to
            a formal identification system. A class of entity for whom the resource is intended or useful. A
            class of entity may be determined by the creator or the publisher
            or by a third party. A comma separated list of persons or organizations which
            supported the creation or maintenance this resource in any way. A date associated with an event in the life cycle of the
            resource. Typically, Date will be associated with the creation or
            availability of the resource. A field that contains additional information about the producer
            of a given ressource. A language of the intellectual content of the resource.
            Recommended best practice for the values of the Language element is
            defined by ISO 639 (en, de, fr, ...) A person or organization owning or managing rights over the
            resource. Recommended best practice is to use the URI or name of
            the Rights Holder to indicate the entity. A reference to a related resource. Recommended best practice is
            to reference the resource by means of a string or number conforming
            to a formal identification system. Access type Additional fields An account of the content of the resource. Description may include
        but is not limited to: an abstract, table of contents, reference to a
        graphical representation of content or a free-text account of the
        content. An entity primarily responsible for making the content of the
            resource. Examples of a Creator include a person, an organization,
            or a service. Typically the name of the Creator should be used to
            indicate the entity. An entity responsible for making contributions to the content of
            the resource. Examples of a Contributor include a person, an
            organization or a service. Typically, the name of a Contributor
            should be used to indicate the entity. An unambiguous reference to the resource within a given context.
        Recommended best practice is to identify the resource by means of a
        string or number conforming to a formal identification system. Arts & Media Audience Clear all Contact Contributor Course Coverage Creator Culture & Society Data protection Date Description Description of technical data Description of the content Display on frontpage Display this resource on the frontpage Economics Economy Email Address* English German Has Technical Problems Have fun browsing the portal eEducation! Health, Sports & Medicine History & Politics Home Image Images Impressum Imprint Information about rights held in and over the resource. Typically
            a Rights element will contain a rights management statement for the
            resource, or reference a service providing such information. Rights
            information often encompasses Intellectual Property Rights (IPR),
            Copyright, and various Property Rights. If the rights element is
            absent, no assumptions can be made about the status of these and
            other rights with respect to the resource. Internal Server Error Is there a link to the resource on the web? Language Language & Literature Languages Law Learning & Teaching Learning resource Learning resources License unexplained Lifecycle Limitation of liability Links Login required Looks like something went wrong! Message Sent Message succesfully sent! Nature & Engineering Next No URL yet. No login required No results found. Noncommercial reuse allowed Noncommercial reuse with modification allowed Online educational resources from the University of Basel Ooops!!! 500 Open resource Page Not Found Paste in you Youtube, Vimeo, or nmc video url People and rights Previous Production Psychology Published Publisher Relation Reuse allowed Reuse with modification allowed Rights Rights unknown Search Sorry, but the page you were trying to view does not exist. Source Subject Subjects Submit Subtitle Supporters Technical issues Thank you for your message. We will get back to you as soon as possible. The entity responsible for making the resource available.
            Examples of a Publisher include a person, an organization, or a
            service. Typically, the name of a Publisher should be used to
            indicate the entity. The extent or scope of the content of the resource. Coverage will
        typically include spatial location (a place name or geographic
        co-ordinates), temporal period (a period label, date, or date range)
        or jurisdiction (such as a named administrative entity). Recommended
        best practice is to select a value from a controlled vocabulary The name given to the resource. Typically, a Title will be a name by
        which the resource is formally known. The nature or genre of the content of the resource. Type includes
            terms describing general categories, functions, genres, or
            aggregation levels for content. Recommended best practice is to
            select a value from a controlled vocabulary (for example, the
            DCMIType vocabulary ).  To describe the physical or digital
            manifestation of the resource, use the FORMAT element. The physical or digital manifestation of the resource. Typically,
            Format may include the media-type or dimensions of the resource.
            Examples of dimensions include size and duration. Format may be
            used to determine the software, hardware or other equipment needed
            to display or operate the resource. The topic of the content of the resource. Typically, a Subject will
        be expressed as keywords or key phrases or classification codes that
        describe the topic of the resource. Recommended best practice is to
        select a value from a controlled vocabulary or formal classification
        scheme. Theology & Religion This field is required. This resource has technical problems. (It uses outdated technology,
        old browser plugins, etc.) This resource may have technical issues. It depends on outdated software or may contain artifacts that are not available to your device. Title Title Image Toggle navigation Type Types University of Basel Unknown Welcome! When you access our webpages, the following data is stored in log files: IP address, date, time, browser request and general information on the operating system and browser. This usage data forms the basis for statistical, anonymous evaluations for the purpose of tracking trends so that the Educational Technologies can improve the information on offer.
         Year animation arts & media audio contributor course creator culture & society date departement department eEducation Portal ebook economics economy english formMessage* formSubject* game german health, sports & medicine history & politics image language & literature languages law learning & teaching learningResources license unexplained nature & engineering psychology rights simulation subject the id of the resource through which it can be syncronised with
            aleph the id of the resource through which it can be syncronised with
            swissbib theology & religion trainer tutorial types video Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
        <p>
        <b>Möchten Sie uns Feedback zum Portal eEducation geben?</b> Möchten auch Sie, dass Ihre oder eine von Ihnen genutzte und geschätzte Online-Ressource ins Portal aufgenommen und darüber auffindbar gemacht wird? Möchten Sie Informationen zum Thema eEducation erhalten? – Dann schreiben Sie uns bitte. Wir freuen uns auf Ihre Nachricht. 
<p>Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos oder anderen Dateien auf der Website gehören ausschliesslich der Universität Basel oder den speziell genannten Rechtsinhabern. Für die Reproduktion jeglicher Elemente ist die schriftliche Zustimmung der Urheberrechtsträger im Voraus einzuholen.</p>

         
Das Herunterladen oder kopieren einer Ressource ändert nichts an den Rechten des Inhalts. 
Wenn sie eine Ressource dieses Portals nutzen möchten, berücksichtigen Sie bitte die Lizenzbestimmungen, unter der sie veröffentlich wurde. 
Der Autor übernimmt keinerlei Gewähr hinsichtlich der inhaltlichen Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und Vollständigkeit der Informationen.         
Gestützt auf Artikel 13 der schweizerischen Bundesverfassung und die datenschutzrechtlichen Bestimmungen des Bundes (Datenschutzgesetz, DSG) hat jede Person Anspruch auf Schutz ihrer Privatsphäre sowie auf Schutz vor Missbrauch ihrer persönlichen Daten. Wir halten diese Bestimmungen ein. Persönliche Daten werden streng vertraulich behandelt und weder an Dritte verkauft noch weiter gegeben.                 
In enger Zusammenarbeit mit unseren Hosting-Providern bemühen wir uns, die Datenbanken so gut wie möglich vor fremden Zugriffen, Verlusten, Missbrauch oder vor Fälschung zu schützen.         
Haftungsansprüche gegen Bildungstechnologien wegen Schäden materieller oder immaterieller Art, welche aus dem Zugriff oder der Nutzung bzw. Nichtnutzung der veröffentlichten Informationen, durch Missbrauch der Verbindung oder durch technische Störungen entstanden sind, werden ausgeschlossen.         
Bildungstechnologien behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen. 
Universität Basel<br>
Bildungstechnologien<br>
Petersgraben 35<br>
4001 Basel<br>
Schweiz<br>
<br>
Tel.: +41 61 207 12 12<br>
E-Mail: bbit@unibas.ch
           
<p>Verweise und Links auf Webseiten Dritter liegen ausserhalb unseres Verantwortungsbereichs Es wird jegliche Verantwortung für solche Webseiten abgelehnt. Der Zugriff und die Nutzung solcher Webseiten erfolgen auf eigene Gefahr des Nutzers oder der Nutzerin.</p>

         
Sie können entweder herumstöbern oder nach Schlüsselwörtern suchen. 
<a href="%(the_url)s">Anregungen</a> zum Portal sind willkommen. <a href="%(the_url)s">Kontaktieren</a> Sie uns, falls auch Ihre Ressource über das Portal eEducation publiziert werden soll. 
Das Portal eEducation ermöglicht den Zugang zu Online Educational Resources der Universität Basel aus verschiedenen Fachrichtungen für alle, die an universitärem Lehren und Lernen interessiert sind.  <h2> Senden Sie uns eine Nachricht </h2> Verweist (vgl. o. Identifier) auf ein Dokument, von dem das aktuell beschriebene Dokument ganz oder in Teilen abgeleitet wurde. Klassifizierung der Zielgruppe des Dokuments, nach Vorgabe des creator, des publisher oder von dritter Seite. Auf Seite des Endrezipienten (z. B. Schüler) beispielsweise nach Fortschritt innerhalb eines Ausbildungsprogramms (educationLevel) oder auf der Gegenseite eine Klassifizierung der Instanz (mediator), die den Zugriff auf das Dokument vermittelt (z. B. Lehrer). Eine Kommagetrennte Liste von Personen und/oder Organisationen, die die Schaffung und/oder Erhaltung dieser Ressource in irgendeiner Weise unterstützt haben. Ein charakteristisches Datum oder auch eine Zeitspanne im Lebenszyklus des Dokuments, sinnvollerweise in Notation nach DIN ISO 8601 als JJJJ-MM-TT, auch im Sinne von Bearbeitungsvermerken und durchaus auch in die Zukunft gerichtet. Im Zweifelsfall das Datum der letzten Dokumentänderung. Ein Feld mit zusätzlichen Informationen zum Produzent. Sprache des Dokumenteninhalts. Empfohlen wird ein Sprachkürzel nach ISO 639 (en,de,fr,...) Name der Person oder Organisation, die Eigner oder Verwerter der Rechte an diesem Dokument ist. Verweist (vgl. o. Identifier) auf ein Dokument, mit dem das beschriebene Dokument in Beziehung steht. Zugriffs Einschränkungen Weiteres Kurzzusammenfassung des Inhalts des Dokuments in Freitext (abstract), Kopie des Inhaltsverzeichnisses bzw. Liste der Bestandteile (tableOfContents) oder als Verweis auf eine beschreibende Quelle. Das Feld wird in Dialogen von bearbeitenden Programmen gerne als „Kommentar“ gelistet. Nach DCMI der Name derjenigen Person oder Organisation, die vorrangig für die Anfertigung des Dokuments verantwortlich ist, also der verantwortliche Verfasser oder Urheber (vgl. source, contributor sowie publisher) des Dokuments in seiner aktuellen Fassung. Das sollte, muss aber nicht, eine natürliche Person sein. Name je einer weiteren Person, die zur Erstellung des Dokuments einen Beitrag geleistet bzw. verantwortet hat. Eindeutige Identifizierung des Dokuments nach einem passenden Katalog, beispielsweise ISBN/ISSN, URL/PURL, URN oder DOI, sowie ggf. den Eintrag (bibliographicCitation), wie das Dokument zu zitieren ist. Kunst & Medien Zielpublikum Auswahl Löschen Kontakt Beitragende Kurs Abdeckung Verfasser Kultur & Gesellschaft Datenschutz Datum Beschreibung Beschreibung technischer Daten Beschreibung des Inhalts Diese Ressource auf der Startseite anzeigen. Diese Ressource auf der Startseite anzeigen. Wirtschaft Wirtschaft Email Adresse* Englisch Deutsch Technische Einschränkungen Wir wünschen Ihnen viel Spass beim Suchen und Stöbern im Portal eEducation. Gesundheit, Sport & Medizin Geschichte & Politik Home Bild Bilder Impressum Impressum Information zur Klarstellung der Rechte, die an dem Dokument gehalten werden oder dies betreffend zu beachten sind, als Direktangabe, in Form eines Verweises (URI) auf Lizenzbedingungen (license) oder Rechteverwerter. Unter (accessRights) wird der Sicherheitsstatus des Dokuments spezifiziert und wer Zugriff darauf hat bzw. haben darf. Interner Serverfehler Gibt es einen Link zu der Resource im Web? Sprache Sprache & Literatur Sprachen Recht Studium & Lehre Ressource Ressourcen Lizenz ungeklärt Lebenszyklus Haftungsausschluss Links Anmeldung erforderlich Hier lief etwas falsch. Nachricht gesendet Nachricht erfolgreich gesendet! Natur & Technik Weiter Noch keine URL vorhanden. Keine Anmeldung erforderlich Keine Resultate gefunden. Nichtkommerzielle Wiederverwendung erlaubt Nichtkommerzielle Wiederverwendung und Veränderung erlaubt Online Educational Resources der Universität Basel Hoppla!!! 500 Ressource öffnen Seite nicht gefunden Youtube, Vimeo, oder NMC Video Url einfügen Personen und Nutzungsrechte Zurück Produktion Psychologie Veröffentlicht Herausgeber Beziehung Wiederverwendung erlaubt Wiederverwendung und Veränderung erlaubt Nutzungsrechte Nutzungsrechte unbekannt Suchen Die von Ihnen angeforderte Seite wurde nicht gefunden. Quelle Fach Fach Senden Untertitel Förderer Technische Einschränkungen Danke für Ihre Nachricht, die wir so schnell wie möglich beantworten werden. Name der veröffentlichenden Instanz, typischerweise der Verleger oder Herausgeber. Auch hier muss nicht, aber sollte die verantwortliche natürliche Person benannt werden. Eingrenzung des vom Dokument inhaltlich abgedeckten Bereichs allgemein, räumlich/örtlich beispielsweise durch Namen nach TGN, auch durch Angabe von Koordinaten (spatial) oder zeitlich (temporal) in Zahlen unter Benennung einer Ära oder Periode. Titel des Dokuments, unter dem es „formell bekannt“ gemacht wird. Das Feld wird von anzeigenden Programmen gerne in den Titelbalken übernommen. Abkürzungen oder auch Übersetzungen des Titels werden alternativ zum formellen Titel (alternative) angegeben. Art bzw. Gattung des Dokuments, bezeichnet am besten mithilfe eines Terminus:Interaktives Dokument („Interactive Resource“), das Benutzereingaben erfordert, z. B. ein Formular.Programm („Software“) als Quelltext oder in ausführbarer Form, soweit es nicht ein interaktives Dokument ist, sondern für die dauerhafte Installation.Datensatz („Dataset“) in spezieller, definierter Kodierung, die für die maschinelle Weiterverarbeitung gedacht ist.Dienste oder Dienstleistungen („Service“), wie ein Copy-Shop oder auch ein Web-Server. Die Formatangabe soll eine Hilfe geben, womit das Dokument dargestellt bzw. weiterverarbeitet werden kann; Größe oder Laufzeit als (extent); Medientyp (medium) als Angabe des physikalischen Datenträgers oder Materials, auf digitale Inhalte bezogen sinnvollerweise als MIME-Typ anzugeben. Thema des Inhalts in suchtauglichen Schlagwörtern (keyword), die bestenfalls einem formalen Klassifikationsschema folgen. Theologie & Religion Dieses Feld ist erforderlich Bei dieser Resource gibt es technische Schwierigkeiten (Sie verwendet veraltete Technologien, Browser Plugins o.ä.).. Bei dieser Ressource gibt es technische Schwierigkeiten. Sie verwendet veraltete Technologien, Browser Plugins o.ä.. Titel Titelbild Navigation umschalten Format Rubriken Universität Basel Nutzungsrechte unbekannt Willkommen! Beim Zugriff auf unsere Webseiten werden folgende Daten in Logfiles gespeichert: IP-Adresse, Datum, Uhrzeit, Browser-Anfrage und allg. übertragene Informationen zum Betriebssystem resp. Browser. Diese Nutzungsdaten bilden die Basis für statistische, anonyme Auswertungen, so dass Trends erkennbar sind, anhand derer wir unsere Angebote entsprechend verbessern können.         Jahr Animation Kunst & Medien Audio Beitragende Kurs Verfasser Kultur & Gesellschaft Datum Fachabteilung Fachabteilung Portal eEducation eBook Wirtschaft Wirtschaft Englisch Nachricht* Betreff* Spiel Deutsch Gesundheit, Sport & Medizin Geschichte & Politik Bild Sprache & Literatur Sprachen Recht Studium & Lehre Ressourcen Lizenz ungeklärt Natur & Technik Psychologie Nutzungsrechte Simulation Fach ID für die Synchronisierung über aleph ID für die Synchronisierung über swissbib Theologie & Religion Trainer Tutorial Format Video 