from .models import LearningResource
from linkcheck import Linklist


class LRLinklist(Linklist):
    model = LearningResource
    url_fields = {"link"}

linklists = {'Pages': LRLinklist}
