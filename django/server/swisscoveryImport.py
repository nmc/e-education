# from types import *
from collections import OrderedDict

import urllib.request, urllib.error, urllib.parse
from django.db.utils import IntegrityError
from html import unescape
import datetime
import re

from server.models import LearningResource, Type, Language,\
                                        Rights, Subject
from django.template.defaultfilters import slugify
from server.swissbibmappings import subject_mappings,\
                                                  type_mappings

class SruDataImporter:
    """."""

    learningResources = []

    def __init__(self, dataDict, importSingleItem=False):
        """."""
        self.dataDict = dataDict

        for record in self.dataDict:
            # len dataDict = 42
            self.extractLearningResources(record)

    def getSubFieldContent(self, subfields, code, delimiter=""):
        """."""
        content = ""
        if isinstance(subfields, OrderedDict):
            # The element contains only one subelement
            content = subfields["#text"] + delimiter
        else:
            # The element contains multiple subelements
            for subfield in subfields:
                # Even the subelement is a list:
                if type(subfield) is list:
                    for subfield_item in subfield:
                        if subfield_item.get("@code") == code:
                            content += subfield[0]["#text"] + delimiter
                # The subelement is the item we are looking for:
                elif subfield.get("@code") == code:
                    content += subfield["#text"] + delimiter
        return content

    def extractLearningResources(self, vi):
        """."""
        _learningResource = {}
        _learningResource["identifier"] = ""
        _learningResource["title"] = ""
        _learningResource["subtitle"] = ""
        _learningResource["subject"] = ""
        _learningResource["coverage"] = ""
        _learningResource["description"] = ""
        _learningResource["format"] = ""
        _learningResource["type"] = ""
        _learningResource["language"] = ""
        _learningResource["creator"] = ""
        _learningResource["publisher"] = ""
        _learningResource["contributor"] = ""
        _learningResource["rights"] = ""
        _learningResource["rightsHolder"] = ""
        _learningResource["source"] = ""
        _learningResource["relation"] = ""
        _learningResource["audience"] = ""
        _learningResource["date"] = datetime.datetime.today()
        _learningResource["link"] = ""
        _learningResource["oai_id"] = ""
        _learningResource["sru_id"] = ""

        # record = vi['recordData']['record']
        record = vi
        for vj in record['datafield']:
            # DOI, etc.
            if (vj["@tag"] == "024"):
                _learningResource["identifier"] \
                    += self.getSubFieldContent(vj["subfield"], "a")
            # Languages
            if (vj["@tag"] == "041"):
                _learningResource["language"] \
                    += self.getSubFieldContent(vj["subfield"], "a", ", ")
                _learningResource["language"] \
                    += self.getSubFieldContent(vj["subfield"], "e", ", ")
            # Creator
            elif (vj["@tag"] == "100"):
                _learningResource["creator"] \
                    += self.getSubFieldContent(vj["subfield"], "D", " ")
                _learningResource["creator"] \
                    += self.getSubFieldContent(vj["subfield"], "a")
            elif (vj["@tag"] == "110"):
                _learningResource["creator"] \
                    += self.getSubFieldContent(vj["subfield"], "a", ", ")
                _learningResource["creator"] \
                    += self.getSubFieldContent(vj["subfield"], "b")
            # Title
            elif (vj["@tag"] == "245"):
                _learningResource["title"] \
                    += self.getSubFieldContent(vj["subfield"], "a")
                # TODO: gehören die folgenden auskommentierten Felder
                # wirklich zum Titel??!
                # _learningResource["title"] \
                # += self.getSubFieldContent(vj["subfield"],"b",", ")
            # Subtitle
            elif (vj["@tag"] == "246"):
                _learningResource["subtitle"] \
                    += self.getSubFieldContent(vj["subfield"], "a")
                # _learningResource["title"] \
                # += self.getSubFieldContent(vj["subfield"],"b")
            # Publisher & Date
            elif (vj["@tag"] == "264"):
                _learningResource["publisher"] \
                    += self.getSubFieldContent(vj["subfield"], "b")
                date_string = self.getSubFieldContent(vj["subfield"], "c")
                # extract year
                m = re.search('\d\d\d\d', date_string)
                if m:
                    print((m.group(0)))
                    _learningResource["date"] \
                        = datetime.datetime(int(m.group(0)), 1, 1)
            # Type
            elif (vj["@tag"] == "500"):
                _learningResource["type"] \
                    = self.getSubFieldContent(vj["subfield"], "a", ", ")
            # Description
            elif (vj["@tag"] == "520"):
                _learningResource["description"] \
                    += self.getSubFieldContent([vj["subfield"]], "a")
            # Rights
            elif (vj["@tag"] == "540"):
                _learningResource["rights"] \
                    += self.getSubFieldContent(vj["subfield"], "a")
            # Contributor
            elif (vj["@tag"] == "700"):
                _learningResource["contributor"] \
                    += self.getSubFieldContent(vj["subfield"], "D", " ")
                _learningResource["contributor"] \
                    += self.getSubFieldContent(vj["subfield"], "a", ", ")
            elif (vj["@tag"] == "710"):
                _learningResource["contributor"] \
                    += self.getSubFieldContent(vj["subfield"], "a", " ")
                _learningResource["contributor"] \
                    += self.getSubFieldContent(vj["subfield"], "b", ", ")
            # Link
            elif (vj["@tag"] == "856"):
                _learningResource["link"] \
                    += self.getSubFieldContent(vj["subfield"], "u")
            # Fachcodes IDSBB (siehe babette)
            # elif (vj["@tag"] == "072"):
            elif (vj["@tag"] == "912"):  # 072 seems to be the wrong code
                _learningResource["subject"] \
                    += self.getSubFieldContent(vj["subfield"], "a", ", ")

        for vj in record['controlfield']:
            # SRU ID
            if (vj["@tag"] == "001"):
                _learningResource["sru_id"] += vj["#text"]
            # Language
            if (vj["@tag"] == "008"):
                _learningResource["language"] += vj["#text"][35:38]

        # for value in _learningResource.itervalues():
        #    if type(value) == str:
        #        value.replace('"','\\"')

        self.learningResources.append(_learningResource)

    def importArrayOfObjects(self):
        """."""
        for lr in self.learningResources:
            self.importSingleObjectFromArray(lr)

    def importSingleObjectFromArray(self, lR):
        """."""
        # Prepare multiple types
        # ----------------------
        _types = []
        # Remove Decoration string
        _csv_types = lR["type"].replace("Technisch-didaktisches Format:", "")
        for _type_string in _csv_types.split(","):
            # If _type_string is shorter than two chars ommit it
            # (sometimes whitespaces are added to the list)
            if len(_type_string) < 2:
                continue

            _clean_english_type_string = ""
            _clean_type_string = _type_string.strip()
            try:
                _clean_english_type_string = type_mappings[_clean_type_string]
            except KeyError:
                _clean_english_type_string = _clean_type_string
            # Get the type if it exists, otherwise strip whitespaces
            # and create it
            _type, _type_created = Type.objects\
                .update_or_create(title=_clean_english_type_string)

            # Append _type to _types list
            _types.append(_type)

        # Prepare subjects
        # ----------------
        _subjects = []
        _csv_subjects = lR["subject"]
        for _subject_code in _csv_subjects.split(","):
            # If _subject_string is shorter than two chars ommit it
            # (sometimes whitespaces are added to the list)
            _subject_string = ""
            if len(_subject_code) < 2:
                continue
            _subject_code = _subject_code.strip()
            try:
                _subject_string = subject_mappings[_subject_code][1]
            except KeyError:
                print(("Key {code} not found.".format(code=_subject_code)))

            # Don't add empty entries
            if _subject_string == "":
                continue

            for _subject_string_clean in _subject_string.split(";"):
                _subject, _subject_created = Subject.objects\
                    .update_or_create(title=_subject_string_clean)

                # Append _subject to _subjects list
                _subjects.append(_subject)

        # If subjects array is empty, add standard entry
        if len(_subjects) == 0:
            _subject_string = "Learning & Teaching"
            _subject, _subject_created = Subject.objects\
                .update_or_create(title=_subject_string)
            _subjects.append(_subject)

        # Prepare multiple languages
        # --------------------------
        _languages = []
        _csv_languages = lR["language"]

        for _language_code in _csv_languages.split(","):
            # If _language_string is shorter than two chars, or if the
            # language is already stored in the _languages array, ommit it.
            # (sometimes whitespaces are added to the list)
            if (len(_language_code) < 2 and
                len([x for x in _languages if x.short_code ==
                    _language_code.strip()]) > 0):
                continue

            # Get the language if it exists, otherwise create it
            _language, _language_created = Language\
                .objects.update_or_create(short_code=_language_code.strip())

            # Append _type to _types list
            _languages.append(_language)

        # Prepare rights
        # --------------
        # Get the rights if it exists, otherwise create it
        _rights = None
        if lR["rights"] != "":  # Don't create empty entries
            _rights, _rights_created = Rights\
                .objects.update_or_create(title=lR["rights"])

        # Prepare title
        # -------------
        # head, sep, tail = lR["title"].rpartition(',')
        # title = head

        # Prepare contributors
        # -------------
        lR["contributor"] = lR["contributor"].strip(' ,')

        # Prepare creators
        # -------------
        lR["creator"] = lR["creator"].strip(' ,')

        # Prepare LearningResource
        # ------------------------
        # Get the LearningResource if it exists, otherwise create it

        # The id of some resources changed while traveling from SwissBib to
        # Swisscovery. If we do not find a resource by swisscovery id, lets check
        # if we find a resource with the swisscovery title. If so, lets assume
        # it is the same resource and replace the SwissBib id by Swisscovery id.
        lrSru = LearningResource.objects.filter(sru_id=lR['sru_id'])
        if len(lrSru) == 0:
            # Once I found '&gt;' characters in an xml, so title comparison did a false negative
            lR['title'] = unescape(lR['title'])
            lrTitle = LearningResource.objects.filter(title=lR['title'])
            if len(lrTitle) > 0:
                print('found resource with old id: {}. replacing {} with {}'.format(lrTitle[0].title, lrTitle[0].sru_id, lR['sru_id']))
                lrTitle[0].sru_id = lR['sru_id']
                lrTitle[0].save()

        # 'Der Patient, den ich nicht verstehe' Problem
        # Original Titel aus Swisscovery: '<<Der Patient>>, den ich nicht verstehe'
        # Dehalb wird die sru_id im Code open nicht mit der neuen SwissCovery id ersetzt.
        # Das führt dazu, das der Code unten versucht, einen neuen Eintrag in die DB einzufügen.
        # Dies führt zu einem Integrity Error, weil der slug (Attribut von
        # Model Learning Resource) unique sein muss, er stimmt aber überein mit dem alten Eintrag.
        # 'der-patient-den-ich-nicht-verstehe' (ohne Dreiecksklammern)
        # Lösung: alten Eintrag löschen, xml importieren.
        try:
            _learningResource, _learningResource_created = LearningResource\
                .objects.update_or_create(
                    sru_id=lR['sru_id'],
                    defaults={
                        "identifier": lR["identifier"],
                        "title": lR["title"],
                        "subtitle": lR["subtitle"],
                        "coverage": lR["coverage"],
                        "description": lR["description"],
                        "format": lR["format"],
                        "creator": lR["creator"],
                        "publisher": lR["publisher"],
                        "contributor": lR["contributor"],
                        "rights": _rights,
                        "rightsHolder": lR["rightsHolder"],
                        "source": lR["source"],
                        "relation": lR["relation"],
                        "audience": lR["audience"],
                        "date": lR["date"],
                        "link": lR["link"],
                        "oai_id": lR["oai_id"],
                        "sru_id": lR["sru_id"]
                    })
        except IntegrityError as e:
            print("ATTENTION: {}".format(e))
            return

        # Add relationships
        # -----------------
        # Add many to many objects
        for _type in _types:
            _learningResource.types.add(_type)

        for _subject in _subjects:
            _learningResource.subjects.add(_subject)

        for _language in _languages:
            _learningResource.languages.add(_language)

        print((("{title} resource created: {created}"
               .format(title=_learningResource.title,
                       created=_learningResource_created)).encode("utf-8")))
        _learningResource.save()
