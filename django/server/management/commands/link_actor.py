from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.core import mail
from django.conf import settings
import textwrap
import logging
from linkcheck.models import Link

# Get an instance of a logger
logger = logging.getLogger('django')


class Command(BaseCommand):
    def handle(self, *args, **options):
        # run linkchecker
        call_command('checklinks', verbosity=3)

        # evaluat result and send mail if necessary
        broken_links = Link.objects.filter(
            ignore=False, url__status=False
        ).count()

        if broken_links > 0:
            subject = str(broken_links) + " broken links found on eEducation",
            message = textwrap.dedent(
                """
                Dear Heidi

                Broken links on eEducation have been dedected. Please
                see the report under the following link for more details:
                https://e-education.unibas.ch/de/admin/linkcheck/?filters=show_invalid

                Kind regards,

                Your server
                """
            )

            email = mail.EmailMessage(subject, message,
                                      "notifications-nmc@unibas.ch",
                                      ['cristoffelmarcus.gehring@unibas.ch', 'bbit@unibas.ch']
                                      )

            email.send()
            logger.info('linkchecker run finished and email sent.')
