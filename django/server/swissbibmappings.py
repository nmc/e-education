# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

# Swissbib to eEducation mappings

# 1 History & Politics
# 2 Health, Sports & Medicine
# 3 Culture & Society
# 4 Arts & Media
# 5 Nature & Engineering
# 6 Psychology
# 7 Law
# 8 Language & Literature
# 9 Learning & Teaching
# 10 Theology & Religion
# 11 Economics

# 1 Geschichte & Politik
# 2 Gesundheit, Sport & Medizin
# 3 Kultur & Gesellschaft
# 4 Kunst & Medien
# 5 Natur & Technik
# 6 Psychologie
# 7 Recht
# 8 Sprache & Literatur
# 9 Studium & Lehre
# 10 Theologie & Religion
# 11 Wirtschaft

subject_mappings = {
    'aa': ('Studium & Lehre', 'Learning & Teaching'),
    'ac': ('Kunst & Medien', 'Arts & Media'),
    'ad': ('Sprache & Literatur', 'Language & Literature'),
    'ae': ('Geschichte & Politik', 'History & Politics'),
    'af': ('Kultur & Gesellschaft', 'Culture & Society'),
    'ag': ('Geschichte & Politik', 'History & Politics'),
    'ak': ('Kunst & Medien', 'Arts & Media'),
    'al': ('Sprache & Literatur', 'Language & Literature'),
    'am': ('Geschichte & Politik', 'History & Politics'),
    'an': ('Sprache & Literatur', 'Language & Literature'),
    'ao': ('Geschichte & Politik', 'History & Politics'),
    'ap': ('Sprache & Literatur', 'Language & Literature'),
    'ar': ('Geschichte & Politik', 'History & Politics'),
    'as': ('Sprache & Literatur', 'Language & Literature'),
    'at': ('Natur & Technik', 'Nature & Engineering'),
    'aw': ('Geschichte & Politik', 'History & Politics'),
    'ba': ('Natur & Technik', 'Nature & Engineering'),
    'bb': ('', ''),
    'bi': ('Natur & Technik', 'Nature & Engineering'),
    'bo': ('Natur & Technik', 'Nature & Engineering'),
    'bu': ('', ''),
    'bw': ('Wirtschaft', 'Economics'),
    'ch': ('Natur & Technik', 'Nature & Engineering'),
    'da': ('Sprache & Literatur', 'Language & Literature'),
    'dh': ('Kultur & Gesellschaft; Natur & Technik', 'Culture & Society; Nature & Engineering'),
    'dk': ('Kunst & Medien', 'Arts & Media'),
    'dl': ('Sprache & Literatur', 'Language & Literature'),
    'dm': ('Sprache & Literatur', 'Language & Literature'),
    'dn': ('Sprache & Literatur', 'Language & Literature'),
    'ds': ('Sprache & Literatur', 'Language & Literature'),
    'es': ('Sprache & Literatur', 'Language & Literature'),
    'et': ('Geschichte & Politik', 'History & Politics'),
    'fa': ('', ''),
    'ﬂ': ('Sprache & Literatur', 'Language & Literature'),
    'fm': ('Kunst & Medien', 'Arts & Media'),
    'fr': ('Sprache & Literatur', 'Language & Literature'),
    'fs': ('Sprache & Literatur', 'Language & Literature'),
    'fu': ('Sprache & Literatur', 'Language & Literature'),
    'ga': ('Geschichte & Politik', 'History & Politics'),
    'ge': ('Natur & Technik', 'Nature & Engineering'),
    'gg': ('Natur & Technik', 'Nature & Engineering'),
    'gh': ('Kultur & Gesellschaft', 'Culture & Society'),
    'gn': ('Kultur & Gesellschaft', 'Culture & Society'),
    'gs': ('Sprache & Literatur', 'Language & Literature'),
    'gw': ('Kultur & Gesellschaft', 'Culture & Society'),
    'hh': ('', ''),
    'hs': ('', ''),
    'hw': ('', ''),
    'id': ('', ''),
    'if': ('Natur & Technik', 'Nature & Engineering'),
    'ig': ('Sprache & Literatur', 'Language & Literature'),
    'ii': ('Sprache & Literatur', 'Language & Literature'),
    'ip': ('Sprache & Literatur', 'Language & Literature'),
    'ir': ('Sprache & Literatur', 'Language & Literature'),
    'is': ('Theologie & Religion', 'Theology & Religion'),
    'it': ('Sprache & Literatur', 'Language & Literature'),
    'ju': ('Kultur & Gesellschaft', 'Culture & Society'),
    'ka': ('Geschichte & Politik', 'History & Politics'),
    'kh': ('Geschichte & Politik', 'History & Politics'),
    'kn': ('Geschichte & Politik', 'History & Politics'),
    'kp': ('Sprache & Literatur', 'Language & Literature'),
    'ks': ('Geschichte & Politik', 'History & Politics'),
    'le': ('Sprache & Literatur', 'Language & Literature'),
    'lm': ('Sprache & Literatur', 'Language & Literature'),
    'ln': ('Sprache & Literatur', 'Language & Literature'),
    'lw': ('Natur & Technik', 'Nature & Engineering'),
    'ma': ('', ''),
    'mb': ('', ''),
    'me': ('Gesundheit, Sport & Medizin', 'Health, Sports & Medicine'),
    'mg': ('Gesundheit, Sport & Medizin', 'Health, Sports & Medicine'),
    'mh': ('Geschichte & Politik', 'History & Politics'),
    'mm': ('Kunst & Medien', 'Arts & Media'),
    'mn': ('Sprache & Literatur', 'Language & Literature'),
    'ms': ('', ''),
    'mt': ('Natur & Technik', 'Nature & Engineering'),
    'mu': ('Kunst & Medien', 'Arts & Media'),
    'nd': ('Sprache & Literatur', 'Language & Literature'),
    'ng': ('Geschichte & Politik', 'History & Politics'),
    'nl': ('Sprache & Literatur', 'Language & Literature'),
    'np': ('Sprache & Literatur', 'Language & Literature'),
    'nu': ('Kultur & Gesellschaft', 'Culture & Society'),
    'nw': ('Natur & Technik', 'Nature & Engineering'),
    'og': ('Geschichte & Politik', 'History & Politics'),
    'or': ('Kultur & Gesellschaft', 'Culture & Society'),
    'pa': ('Kultur & Gesellschaft', 'Culture & Society'),
    'pf': ('Kultur & Gesellschaft', 'Culture & Society'),
    'ph': ('Kultur & Gesellschaft', 'Culture & Society'),
    'pm': ('Natur & Technik', 'Nature & Engineering'),
    'po': ('Geschichte & Politik', 'History & Politics'),
    'pp': ('Sprache & Literatur', 'Language & Literature'),
    'pr': ('', ''),
    'ps': ('Psychologie', 'Psychology'),
    'py': ('Natur & Technik', 'Nature & Engineering'),
    'ro': ('Theologie & Religion', 'Theology & Religion'),
    'rr': ('Sprache & Literatur', 'Language & Literature'),
    'rs': ('Sprache & Literatur', 'Language & Literature'),
    'ru': ('Sprache & Literatur', 'Language & Literature'),
    'rw': ('Recht', 'Law'),
    'sa': ('Kultur & Gesellschaft', 'Culture & Society'),
    'se': ('Sprache & Literatur', 'Language & Literature'),
    'sg': ('Geschichte & Politik', 'History & Politics'),
    'sk': ('Sprache & Literatur', 'Language & Literature'),
    'sl': ('Sprache & Literatur', 'Language & Literature'),
    'sm': ('Sprache & Literatur', 'Language & Literature'),
    'so': ('Kultur & Gesellschaft', 'Culture & Society'),
    'sp': ('Gesundheit, Sport & Medizin', 'Health, Sports & Medicine'),
    'ss': ('Sprache & Literatur', 'Language & Literature'),
    'te': ('Natur & Technik', 'Nature & Engineering'),
    'th': ('Theologie & Religion', 'Theology & Religion'),
    'tu': ('Sprache & Literatur', 'Language & Literature'),
    'ur': ('Geschichte & Politik', 'History & Politics'),
    'uw': ('Natur & Technik', 'Nature & Engineering'),
    'vk': ('Kultur & Gesellschaft', 'Culture & Society'),
    'vm': ('Natur & Technik', 'Nature & Engineering'),
    'vw': ('Wirtschaft', 'Economics'),
    'wa': ('Kultur & Gesellschaft', 'Culture & Society'),
    'wf': ('Kultur & Gesellschaft', 'Culture & Society'),
    'wg': ('Wirtschaft; Kultur & Gesellschaft', 'Economics; Culture & Society'),
    'wi': ('Wirtschaft; Kultur & Gesellschaft', 'Economics; Culture & Society'),
    'ws': ('Sprache & Literatur', 'Language & Literature'),
    'yi': ('Sprache & Literatur', 'Language & Literature'),
    'za': ('Kultur & Gesellschaft', 'Culture & Society'),
    'zm': ('Gesundheit, Sport & Medizin', 'Health, Sports & Medicine'),
    'zo': ('Natur & Technik', 'Nature & Engineering'),
    'zz': ('Verschiedenes',)
}

type_mappings = {
    'Animation': 'animation',
    'Audio': 'audio',
    'Bild': 'image',
    'Foto': 'image',  # Type Foto shoudn't exist actually, this is a work
                      # around to prevent the importer from creating
                      # deprecated types
    'eBook': 'ebook',
    'Tutorial': 'tutorial',
    'Kurs': 'course',
    'Simulation': 'simulation',
    'Spiel': 'game',
    'Trainer': 'trainer',
    'Video': 'video',
}
