from django.utils.translation import ugettext_lazy as _
from django.utils.translation import pgettext_lazy

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout, Submit, Div, HTML

import floppyforms as forms

from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox

from haystack.forms import FacetedSearchForm

class SwisscoveryForm(forms.Form):
    file = forms.FileField()
    def __init__(self, *args, **kwargs):
        super(SwisscoveryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        # self.helper.label_class = 'sr-only'
        self.helper.form_show_labels = False
        self.helper.wrapper_class = 'width-100'
        self.helper.form_class = 'width-100 contact-form'
        self.helper.layout = Layout(
            Div(
                Div(
                    HTML(_('<h2> Upload Resources from a Swisscovery xml file </h2>')),
                    css_class="text"
                ),
                Div(
                    Field('file', placeholder=_("File*")),
                    # for field_name, field in self.fields.items():
                    css_class="col-xs-12 col-sm-8 contact-form-groups",
                ),
                Div(
                    Submit('submit', _('Submit')),
                ),

                css_class="col-xs-12 col-sm-4 contact-form-header"
            ),
        )

class ContactForm(forms.Form):

    name = forms.CharField(required=True, label='Name')
    email = forms.EmailField(required=True, label='Email')
    subject = forms.CharField(required=True, label='Subject')
    message = forms.CharField(widget=forms.Textarea, label='Message')
    captcha = ReCaptchaField(
        widget = ReCaptchaV2Checkbox(attrs={'size': 'compact'})
    )

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        # self.helper.label_class = 'sr-only'
        self.helper.form_show_labels = False

        # self.helper.field_class = 'col-xs-12 col-sm-9'
        self.helper.wrapper_class = 'width-100'
        self.helper.form_class = 'width-100 contact-form'
        self.helper.layout = Layout(
            Div(
                Div(
                    HTML(_('<h2> Send us a message </h2>')),
                    css_class="text"
                ),
                css_class="col-xs-12 col-sm-4 contact-form-header"
            ),

            Div(
                Field('name', placeholder=_("Name*")),
                Field('email', placeholder=_("Email Address*")),
                Field('subject',
                      placeholder=pgettext_lazy("form", "Subject*")),
                Field('message',
                      placeholder=pgettext_lazy("form", "Message*")),
                Field('captcha'),
                Div(Submit('submit', _('Submit')),),
                # for field_name, field in self.fields.items():
                css_class="col-xs-12 col-sm-8 contact-form-groups",
            )
        )
        # self.helper.add_input(Submit('submit', 'Submit'))


class ImportForm(forms.Form):
    sru_id = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))
        super(ImportForm, self).__init__(*args, **kwargs)


class PopulatedFacetedSearchForm(FacetedSearchForm):
    def no_query_found(self):
        """
        Determines the behavior when no query was found.

        By default, no results are returned (``EmptySearchQuerySet``).

        Should you want to show all results, override this method in your own
        ``SearchForm`` subclass and do ``return self.searchqueryset.all()``.
        """
        return self.searchqueryset.all().order_by('-popularity')
