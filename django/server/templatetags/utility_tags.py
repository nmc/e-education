import ast
from django import template
from django.utils import translation
from django.urls import reverse, resolve

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

register = template.Library()

class TranslatedURL(template.Node):
    def __init__(self, language):
        self.language = language

    def render(self, context):
        try:
            view = resolve(context['request'].path)
            request_language = translation.get_language()
            translation.activate(self.language)
            url = reverse(view.url_name, args=view.args, kwargs=view.kwargs)
            translation.activate(request_language)
            return url
        except:
            return ''


@register.tag(name='translate_url')
def do_translate_url(parser, token):
    language = token.split_contents()[1]
    return TranslatedURL(language)


@register.simple_tag
def get_random_color():
    '''
    Return a random hex color string
    '''
    import random
    color = "%06x" % random.randint(0, 0xFFFFFF)
    return color


@register.simple_tag
def facet_checked(facet_name, get_parameters):
    '''
    Return the string 'active' if facet_name is in the parameterlist
    of the request.
    '''
    isFacetActive = ''
    if(len(get_parameters)>0):
        active_facets = dict(get_parameters.lists())
        if "selected_facets" in active_facets:
            for active_facet in active_facets["selected_facets"]:
                if(facet_name == active_facet.split(":")[1]):
                    isFacetActive = 'active'
    return isFacetActive


@register.simple_tag
def sub_facet_checked(facet_name, get_parameters):
    '''
    Return the string 'active' if a part of facet_name is in
    the parameterlist of the request
    '''
    isFacetActive = ''
    if(len(get_parameters)>0):
        active_facets = dict(get_parameters.lists())
        if "selected_facets" in active_facets:
            for active_facet in active_facets["selected_facets"]:
                if(facet_name in active_facet):
                    isFacetActive = 'active'
    return isFacetActive


@register.simple_tag
def active_facets_list(get_parameters):
    '''
    Return a list of active_facets
    '''
    active_facets = []
    if(len(get_parameters)>0):
        raw_active_facets = dict(get_parameters.lists())
        if "selected_facets" in raw_active_facets:
            for raw_active_facet in raw_active_facets["selected_facets"]:
                active_facets.append(raw_active_facet.split(":")[1])
    return active_facets


@register.simple_tag
def display_icon(icontype, key, lang="en"):
    ICONS = {
        "rights-creative-commons-attribution-noncommercial-no-derivative-works-25-switzerland-licence-en": "/static/images/license-buttons/by-nc-nd.svg",
        "rights-creative-commons-attribution-noncommercial-no-derivative-works-25-switzerland-licence-de": "/static/images/license-buttons/by-nc-nd.svg",
        "rights-cc-by-nc-nd-eu-en": "/static/images/license-buttons/by-nc-nd-eu.svg",
        "rights-cc-by-nc-nd-eu-de": "/static/images/license-buttons/by-nc-nd-eu.svg",
        "rights-cc-by-nc-nd-en": "/static/images/license-buttons/by-nc-nd.svg",
        "rights-cc-by-nc-nd-de": "/static/images/license-buttons/by-nc-nd.svg",
        "rights-cc-by-nc-sa.eu-en": "/static/images/license-buttons/by-nc-sa.eu.svg",
        "rights-cc-by-nc-sa.eu-de": "/static/images/license-buttons/by-nc-sa.eu.svg",
        "rights-cc-by-nc-sa-en": "/static/images/license-buttons/by-nc-sa.svg",
        "rights-cc-by-nc-sa-de": "/static/images/license-buttons/by-nc-sa.svg",
        "rights-cc-by-nc.eu-en": "/static/images/license-buttons/by-nc.eu.svg",
        "rights-cc-by-nc.eu-de": "/static/images/license-buttons/by-nc.eu.svg",
        "rights-cc-by-nc-en": "/static/images/license-buttons/by-nc.svg",
        "rights-cc-by-nc-de": "/static/images/license-buttons/by-nc.svg",
        "rights-cc-by-nd-en": "/static/images/license-buttons/by-nd.svg",
        "rights-cc-by-nd-de": "/static/images/license-buttons/by-nd.svg",
        "rights-cc-by-sa-en": "/static/images/license-buttons/by-sa.svg",
        "rights-cc-by-sa-de": "/static/images/license-buttons/by-sa.svg",
        "rights-cc-by-en": "/static/images/license-buttons/by.svg",
        "rights-cc-by-de": "/static/images/license-buttons/by.svg",
        "rights-cc-0-en": "/static/images/license-buttons/cc-zero.svg",
        "rights-cc-0-de": "/static/images/license-buttons/cc-zero.svg",
        "rights-cc-cc.srr.primary-en": "/static/images/license-buttons/cc.srr.primary.svg",
        "rights-cc-cc.srr.primary-de": "/static/images/license-buttons/cc.srr.primary.svg",
        "rights-rights-unknown-en":"/static/images/text-icons/en-rights-unknown.png",
        "rights-rights-unknown-de":"/static/images/text-icons/de-rights-unknown.png",
        "language-eng-en": "/static/images/language-icons/language-en.png",
        "language-ger-en": "/static/images/language-icons/language-de.png",
        "language-fre-en": "/static/images/language-icons/language-fr.png",
        "language-spa-en": "/static/images/language-icons/language-es.png",
        "type-animation-en": "/static/images/type-icons/animation.png",
        "type-audio-en": "/static/images/type-icons/audio.png",
        "type-course-en": "/static/images/type-icons/course.png",
        "type-ebook-en": "/static/images/type-icons/eBook.png",
        "type-game-en": "/static/images/type-icons/game.png",
        "type-image-en": "/static/images/type-icons/image.png",
        "type-simulation-en": "/static/images/type-icons/simulation.png",
        "type-tutorial-en": "/static/images/type-icons/tutorial.png",
        "type-trainer-en": "/static/images/type-icons/trainer.png",
        "type-video-en": "/static/images/type-icons/video.png",
        "tech-issue-de": "/static/images/text-icons/de-technical-issues.png",
        "tech-issue-en": "/static/images/text-icons/en-technical-issues.png",
        "login-required-de": "/static/images/text-icons/de-login-required.png",
        "login-required-en": "/static/images/text-icons/en-login-required.png",
    }
    try:
        return ICONS[icontype + "-" + key + "-" + lang]
    except KeyError:
        return ""



@register.simple_tag
def display_explanation(key, lang="en"):
    EXPLANATIONS = {
        "types-en": "Type(s) of the resource",
        "types-de": "Ressourcentyp",
        "subject-en": "Subject(s) of the resource",
        "subject-de": "Fachbereich",
        "rights-en": "How you may use this resource",
        "rights-de": "Wie Sie diese Ressource weiterverwenden dürfen",
        "languages-en": "Language(s) of the resource",
        "languages-de": "Sprache in der die Ressource verfasst wurde",
    }
    try:
        return EXPLANATIONS[key + "-" + lang]
    except KeyError:
        return ""



# the haystack/xapian returns a string. This string contains either chars for
# the 'rights' facet or a list. lets convert the string to a python object,
# either string or a list.
def convertStringToPyhtonObject(string):
    # ast failes if it can't find objects (as lists) in the string. E.g. our
    # 'rights' facet. We have to catch that exception:
    try:
        x = ast.literal_eval(string)
    except:
        return [string]
    else:
        return x

# we need the facets in a different format than delivered by haystack/xapian
# delivered: [("['english', 'spanish']", 1), ("['english']", 16), ("['german', 'english', 'french']", 1), ("['german', 'english']", 7), ("['german', 'french']", 2), ("['german']", 27)]
# wanted: [('french', 3), ('german', 37), ('english', 25), ('spanish', 1)]
@register.simple_tag
def format_facet_list(result):
    facetSet = set([])
    purePythonResult = []
    for res in result:
        facetList = convertStringToPyhtonObject(res[0])
        count = res[1]
        purePythonResult.append((facetList, count))
        for f in facetList:
            facetSet.add(f)
    newFormat = []
    for f in facetSet:
        count = 0
        for r in purePythonResult:
            if f in r[0]:
                count += r[1]
        newFormat.append((f,count))
    return newFormat

@register.filter(name='sort')
def listsort(value, rev=False):
    if isinstance(value, dict):
        new_dict = SortedDict()
        key_list = value.keys()
        key_list.sort(reverse=rev)
        for key in key_list:
            new_dict[key] = value[key]
        return new_dict
    elif isinstance(value, list):
        new_list = list(value)
        new_list.sort(reverse=rev)
        return new_list
    else:
        return value
listsort.is_safe = True


@register.filter
def cleanup_facets(facets):
    filtered_facets = []

    for facet in facets:
        # Filter facets
        not_null_facets = []
        for facet_value in facet[1]:
            if facet_value[1] > 0:
                not_null_facets.append(facet_value)
        # If there is at least one facet with more than zero results, append it
        if len(not_null_facets) > 0:
            filtered_facets.append((facet[0], not_null_facets))

    # Remove facets that have less than two options
    filtered_facets = filter(lambda x: len(x[1]) >= 2, filtered_facets)

    return filtered_facets
