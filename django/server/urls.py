from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import TemplateView
from django.views import defaults as default_views

from haystack.generic_views import FacetedSearchView

from server.views import home_files
from server.views import autocomplete
from server.views import ContactFormView, SwisscoveryFormView
from server.views import HomeView
from server.views import SearchView
from server.views import AccessCountDetailView
from server.models import LearningResource
from server.forms import ContactForm, SwisscoveryForm
from server.forms import PopulatedFacetedSearchForm

import linkcheck

# Add the files which shouldn't be internationalized
urlpatterns = [
    re_path(r'^(?P<filename>(robots.txt)|(humans.txt))$',
        home_files, name='home-files'),
    path('search/autocomplete/', autocomplete, name='autocomplete'),
    path('oidc/', include('mozilla_django_oidc.urls')),
]

urlpatterns += i18n_patterns(
    path('',
        HomeView.as_view(
            form_class=PopulatedFacetedSearchForm,
            facet_fields=['languages', 'rights', 'types', 'subject'],
            template_name='search/welcome.html'
        ), name="home"),
    path('about/',
         TemplateView.as_view(template_name='pages/about.html'),
         name="about"
         ),
    path('impressum/',
         TemplateView.as_view(template_name='pages/impressum.html'),
         name="impressum"
         ),

    path('contact/',
         ContactFormView.as_view(
             template_name='pages/contact.html',
             success_url='.',
             form_class=ContactForm
         ),
         name="contact"
         ),
    re_path(r'^resource/(?P<slug>[-\w]+)/$',
            AccessCountDetailView.as_view(
                template_name='pages/resource.html',
                model=LearningResource
            ),
            name="resource"
            ),
    path('search/',
         SearchView.as_view(
            form_class=PopulatedFacetedSearchForm,
            facet_fields=['languages', 'rights', 'types', 'subject'],
            template_name='search/search.html',
        ), name="search"),

    path('admin/linkcheck/', include('linkcheck.urls')),
    path('admin/', admin.site.urls),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path('400/', default_views.bad_request),
        path('403/', default_views.permission_denied),
        path('404/', default_views.page_not_found),
        path('500/', default_views.server_error),
        path('__debug__/', include(debug_toolbar.urls)),
    ]
