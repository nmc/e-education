from haystack import indexes
from haystack.exceptions import SkipDocument
from .models import LearningResource, Rights


# Attribute
#   mit der Eigentschaft faceted=True wird das Feld für Faceten verwendet
#   mit der Eigentschaft boost=X wird festgelegt wie wichtig ein Feld ist
#   (Achtung der Boost wird während der Indizierungszeit angewendet)
#
class LearningResourceIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True,
                              model_attr='title', boost=1.125)
    creator = indexes.CharField(model_attr='creator', null=True)
    subject = indexes.CharField(model_attr='subjects', faceted=True)
    description = indexes.CharField(model_attr='description')
    contributor = indexes.CharField(model_attr='contributor', null=True)
    production = indexes.CharField(model_attr='production', null=True)
    date = indexes.DateTimeField(model_attr='date')
    types = indexes.CharField(model_attr='types', faceted=True)
    languages = indexes.CharField(model_attr='languages', faceted=True)
    rights = indexes.CharField(model_attr='rights', faceted=True, null=True)
    link = indexes.CharField(model_attr='link')
    popularity = indexes.DecimalField(model_attr='popularity')

    # We add this for autocomplete.
    content_auto = indexes.EdgeNgramField(model_attr='title')

    def prepare_rights(self, obj):
        d = dict(Rights.RIGHTS_CHOICES)
        return d[obj.rights.rights_type]

    def prepare_subject(self, obj):
        subjects = []
        for subject in obj.subjects.all():
            subjects.append(subject.title.lstrip())
        return subjects

    def prepare_types(self, obj):
        types = []
        for t in obj.types.all():
            types.append(t.title.lstrip())
        return types

    def prepare_languages(self, obj):
        languages = []
        for l in obj.languages.all():
            languages.append(l.title.lstrip())
        return languages

    def get_model(self):
        return LearningResource

    # Problem: index_queryset funzt nur, wenn man manage.py rebuild_index update_index macht.
    # Ich habe haystack signaling aktiviert, das pro gespeichertes/gelöschtes
    # Object updates am Index vornimmt -> published=False Objekte werden
    # trotzdem indexiert:
    # Also haystack signaling abstellen, dafür timer systemd einsetzten, der update_index macht
    # def prepare(self, obj):
    #     if obj.published is False:
    #         print("Indexing for object '{}' skipped".format(obj))
    #         # import pdb; pdb.set_trace()
    #         return {}
    #     self.prepared_data = super(LearningResourceIndex, self).prepare(obj)
    #     return self.prepared_data

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(published=True)
