from .models import LearningResource
from django.db import models
from haystack import signals

from django.core import management

def rebuildCallback(sender, **kwargs):
    management.call_command('rebuild_index', interactive=False)

class LRSignalProcessor(signals.BaseSignalProcessor):
    def setup(self):
        # Listen only to the ``LearningResource`` model.
        print("GRRETTINGS FROM setup")
        models.signals.post_save.connect(rebuildCallback, sender=LearningResource)
        models.signals.post_delete.connect(rebuildCallback, sender=LearningResource)

    def teardown(self):
        # Disconnect only for the ``LearningResource`` model.
        print("GRRETTINGS FROM teardown")
        models.signals.post_save.disconnect(rebuildCallback, sender=LearningResource)
        models.signals.post_delete.disconnect(rebuildCallback, sender=LearningResource)
