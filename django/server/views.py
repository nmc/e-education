from django.contrib import messages
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView
from django.views.generic import DetailView

from haystack.query import SearchQuerySet
from haystack.generic_views import FacetedSearchView
import simplejson as json
import urllib.request, urllib.error, urllib.parse


from .forms import ContactForm, SwisscoveryForm
from .models import LearningResource, Type

import smtplib, ssl
from email.mime.text import MIMEText
from django.conf import settings



def handler404(request):
    response = render(None, '404.html', RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render(None, '500.html', RequestContext(request))
    response.status_code = 500
    return response


def home_files(request, filename):
    return render(request, filename, {}, content_type="text/plain")


def autocomplete(request):
    sqs = SearchQuerySet().autocomplete(content_auto=request.GET.get('q',
                                                                     ''))[:5]
    suggestions = [result.object.title for result in sqs]
    # Make sure you return a JSON object, not a bare list.
    # Otherwise, you could be vulnerable to an XSS attack.
    the_data = json.dumps({
        'results': suggestions
    })
    return HttpResponse(the_data, content_type='application/json')
"""
if form.is_valid():
    subject = form.cleaned_data['subject']
    message = form.cleaned_data['message']
    name = form.cleaned_data['name']
    email = form.cleaned_data['email']

    recipients = ['florian.siess@unibas.ch']
    if cc_myself:
        recipients.append(sender)

    send_mail(subject, message, name, email)
    return HttpResponseRedirect('/thanks/')
"""

class ContactFormView(FormView):

    form_class = ContactForm
    def form_valid(self, form):
        message = MIMEText("{name} / {email} said:\n\n{msg}".format(
            name=form.cleaned_data.get('name'),
            email=form.cleaned_data.get('email'),
            msg=form.cleaned_data.get('message')
        ))
        message['Subject'] = form.cleaned_data.get('subject')
        message['From'] = "notifications-nmc@unibas.ch"
        message['To'] = "bbit@unibas.ch, nmc-notifications@unibas.ch"
        # TODO: Change email adresses
        server = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
        context = ssl.SSLContext(ssl.PROTOCOL_TLS)
        server.ehlo()
        server.starttls(context=context) # Secure the connection
        server.ehlo()
        server.sendmail(
            "notifications-nmc@unibas.ch",
            ["bbit@unibas.ch", "notifications-nmc@unibas.ch"],
            message.as_string()
        )
        server.quit
        messages.success(self.request,
                         _("Thank you for your message. We will get back to you as soon as possible."))
        return super(ContactFormView, self).form_valid(form)

class SwisscoveryFormView(FormView):

    form_class = SwisscoveryForm

    def form_valid(self, form):
        xmlFile = form.files["file"]
        fname = xmlFile.name
        from lxml import etree

        xml = xmlFile.read()
        root = etree.fromstring(xml)

        for appt in root.getchildren():
            for elem in appt.getchildren():
                if not elem.text:
                    text = "None"
                else:
                    text = elem.text
                print(elem.tag + " => " + text)
        return super(SwisscoveryFormView, self).form_valid(form)

class SearchView(FacetedSearchView):
    pass
    # def get_queryset(self):
    #     # import pdb; pdb.set_trace()
    #     # queryset = super(FacetedSearchView, self).get_queryset()
    #     return queryset.filter(published=True)

class HomeView(FacetedSearchView):
    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['learningresource_list'] = LearningResource.objects.filter(
                frontpage=True,
                published=True
            ).order_by('date')[:3]
        context['type_list'] = Type.objects.all()
        return context


class AccessCountDetailView(DetailView):
    def get_object(self):
        object = super(AccessCountDetailView, self).get_object()
        object.popularity += 1
        object.save()

        return object
