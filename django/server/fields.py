from django.db import models


class CharNullField(models.CharField):  # subclass the CharField
    """CharField that stores NULL but returns an empty string."""

    description = "CharField that stores NULL but returns an empty string."
    # __metaclass__ = models.SubfieldBase  # ensures to_python will be called

    def to_python(self, value):
        """When loading field from db convert None to empty string."""
        # this is the value right out of the db, or an instance
        # if an instance, just return the instance
        if isinstance(value, models.CharField):
            return value
        if value is None:  # if the db has a NULL (None in Python)
            return ''      # convert it into an empty string
        else:
            return value   # otherwise, just return the value

    def get_prep_value(self, value):  # catches value before sending to db
        """Before saving field to db, convert empty string to None."""
        if value == '':
            # if Django tries to save an empty string, send the db None (NULL)
            return None
        else:
            # otherwise, just pass the value
            return value
