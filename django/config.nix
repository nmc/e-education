{ djangoIP, bridgeIP }: { pkgs, config ? <nixos-config>, ... }: {
  environment.systemPackages = with pkgs; [
    python38
    python38Packages.xapian
    mariadb-client
    libmysqlclient # mariadb-connector-c, adds
    xapian
  ];

  networking.defaultGateway = bridgeIP;
  networking.firewall.allowedTCPPorts = [ 8000 ];
  networking.hostName = "eEducation-django";
  networking.interfaces.eth0.ipv4.addresses = [{
    address = djangoIP;
    prefixLength = 24;
  }];
  networking.nameservers = [ "131.152.1.1" "131.152.1.5" ];
  networking.search = [ "nmc.unibas.ch" ];
  networking.useDHCP = false;

  security.sudo.configFile = "%wheel ALL=(ALL) ALL";

  services.openssh.enable = true;
  services.uwsgi = {
    enable = true;
    plugins = ["python3"];
    instance = {
      type = "normal";
      # uwsgi protocol socket for nginx, configuring nginx to communicate
      # through this port via the wsgi protocol would be faster
      socket = "0.0.0.0:33140";
      # http socket for mailman core to reach the hyperkitty API directly
      pythonPackages = self: with self; [ xapian ];
      http-socket = "0.0.0.0:8000";
      wsgi-file = "/srv/www/vhosts/e-education.unibas.ch/files/django/server/wsgi.py";
      chdir = "/srv/www/vhosts/e-education.unibas.ch/files/django";
      module = "server.wsgi:application";
      master = true;
      processes = 4;
      vacuum = true;
    };
  };
  systemd.services.linkcheck = {
    enable = true;
    description = "Run linkchecker on E-education";
    environment = {
      PYTHONPATH = "/srv/www/vhosts/e-education.unibas.ch/files/venv/lib/python3.8/site-packages:/srv/www/vhosts/e-education.unibas.ch/files/django:/srv/www/vhosts/e-education.unibas.ch/files/secrets";
    };
    serviceConfig = {
      ExecStart = "${pkgs.python38}/bin/python /srv/www/vhosts/e-education.unibas.ch/files/django/manage.py checklinks --settings=config.production";
      Type = "oneshot";
    };
  };
  systemd.timers.linkcheck = {
    enable = true;
    timerConfig = {
      OnCalendar = "*-*-* 7:00:00";
      Unit = "linkcheck.service";
    };
    wantedBy = [ "timers.target" ];
  };
  systemd.tmpfiles.rules = [
    # "z /var/log/httpd - wwwrun wwwrun - -"
    "Z /srv/www/vhosts/e-education.unibas.ch/ 0700 uwsgi uwsgi - -"
  ];

  users = {
    mutableUsers = false;
    users.nmc-schnauz = {
      isNormalUser = true;
      password = "bling";
      home = "/home/nmc-schnauz";
      extraGroups = [ "wheel" ];
    };
  };
}
